
import React, {Component} from 'react';
import {Icon as Icn} from 'react-native-vector-icons/FontAwesome';
import {View} from 'react-native';
import {Root} from 'native-base';
import {RootNavagator} from './src/routs';
export default class App extends Component {
  render() {
    return (
      <Root>
        <RootNavagator />
      </Root>
    );
  }
}

