import React, {Component} from 'react';
import{ StyleSheet, Image, } from 'react-native';
import Modal from "react-native-modal";
import {Container,Header, Left, Button,Text, Right, Content, Drawer, Icon, View, Form, Input, Item, Col, Grid, CardItem, Thumbnail, Card,} from 'native-base';
import {SideBar} from './index';

class ChatRoomComponent extends Component {
  state = {
    isModalVisible: false
  }
  toggleModal = () => {
    this.setState({ isModalVisible: !this.state.isModalVisible });
  }
  closeDrawer=()=>{
      this.drawer._root.close()
  }
  openDrawer=()=>{
      this.drawer._root.open()
  }
  render() {
    const {navigate}=this.props.navigation;
    return (
      <Drawer
        ref={(ref)=>{this.drawer=ref;}}
        content={<SideBar navigate={navigate}/>}
        onClose={()=>this.closeDrawer()}
      >
        <Container>
          <Header searchBar rounded style={{backgroundColor:'#3f4392'}}>
            <Left>
              <Grid style={{paddingTop:'6%'}}>
                <Col style={{width:'15%'}}>
                  <Button style={{marginTop:-5}} transparent title="Show modal" onPress={this.toggleModal} >
                    <Icon name="dots-three-vertical" style={{color:'#FFF',fontSize:22,}} type="Entypo"/>
                  </Button>
                  <Modal isVisible={this.state.isModalVisible}>
                    <View style={{ flex: 1,paddingTop:'30%',}}>
                      <View style={{backgroundColor:'#FFF',borderRadius:20}}>
                        <Button 
                          style={{
                            backgroundColor:'#3f4392',
                            borderRadius:20,
                            borderBottomLeftRadius:0,
                            borderBottomRightRadius:0,
                            width:'100%'
                          }}
                          title="Hide modal" onPress={this.toggleModal} 
                        >
                          <Icon style={{color:'#FFF',fontSize:25,marginLeft: 10,}} name="close" type="AntDesign"/>
                        </Button>
                        <Card style={{paddingBottom:'5%',paddingTop:'5%'}}>
                          <Thumbnail source={require('../img/hs.jpg')} style={{marginLeft:'43%',}}/>
                          <Text style={{textAlign:'center',fontWeight:'bold',marginTop:15}}>سید حسین سعیدی</Text>
                          <Text style={{textAlign:'center',marginTop:5}}>مشاور حقوقی</Text>
                        </Card>
                        <Button full light style={{margin:5}}>
                            <Text style={{color:'#3f4392'}}>جستجو</Text>
                        </Button>
                        <Button full light style={{margin:5}}>
                            <Text style={{color:'#3f4392'}}>پاک کردن تاریخچه</Text>
                        </Button>
                        <Button full light style={{margin:5}}>
                            <Text style={{color:'#3f4392'}}>حذف گفتگو</Text>
                        </Button>
                      </View>  
                    </View>
                  </Modal>
                </Col>
                <Col style={{width:'15%'}}>
                  <Icon name="stopwatch" style={{color:'#FFF',fontSize:25,}} type="Entypo"/>
                </Col>
                <Col style={{width:'70%'}}>
                  <Text note style={{color:'#fff',fontSize:20,marginTop:'4%'}} >00:00</Text>
                </Col>
              </Grid>
            </Left>
            <Right>
              <Col style={{width:'60%',paddingBottom:'5%',paddingRight:2}}>
                <Text note style={{color:'#fff',fontSize:10,marginTop:'6%',textAlign:'right',fontWeight:'bold'}} >سید حسین سعیدی</Text>
                <Text note style={{color:'#fff',fontSize:10,marginTop:'2%',textAlign:'right'}} >مشاور</Text>
              </Col>
              <Col style={{width:'15%',paddingBottom:'5%'}}>
                <Image source={require('../img/chat/user.png')}/>
              </Col>
              <Col style={{width:'25%'}}>
                <Button transparent onPress={()=>this.openDrawer()}>
                  <Icon style={{color:'#FFF'}} name="menu" size={30}/>
                </Button>
              </Col>
            </Right>
          </Header>
          <Content style={[{backgroundColor:'rgba(153,153,153,0.6)',padding:5}]}>
            <View>
              <Grid>
                <Col>
                  <View style={[styles.textStyle,{borderTopLeftRadius:0,backgroundColor:'#eee',}]}>
                    <Text style={{textAlign:'right',textAlign:1}}>سلام خوبین</Text>
                  </View>
                </Col>
                <Col>
                  <View style={[styles.textStyle,{borderTopRightRadius:0,marginLeft:10,backgroundColor:'#ddd',}]}>
                    <Text style={{textAlign:'right',textAlign:1}}>ممنون بفرمایید</Text>
                  </View>
                </Col>
              </Grid>
            </View>
          </Content>
          <View>
            <CardItem style={{borderRadius:50}}>
              <Grid style={{borderRadius:10}}>
                <Col style={{width:'7%'}}>
                  <Button transparent>
                    <Image source={require('../img/chat/send.png')}/>
                  </Button>
                </Col>
                <Col style={{width:'86%'}}>
                  <Form>
                    <Item last>
                      <Input style={{textAlign:'right',}} placeholder="پیام خود را بنویسید..."/>
                    </Item>
                  </Form>
                </Col>
                <Col style={{width:'7%'}}>
                  <Button transparent>
                    <Image source={require('../img/chat/paper-clip.png')}/>
                  </Button>
                </Col>
              </Grid>
            </CardItem>
          </View>
        </Container>
      </Drawer>
    );
  }
}
const styles = StyleSheet.create({
  textStyle:{
    width:'95%',
    padding:'3%',
    borderRadius:10,
    marginTop:3
  },
})
export {ChatRoomComponent};