import React, {Component} from 'react';
import {StyleSheet,Image,View} from 'react-native';
import {Container,Header, Left, Button,Text, Card, Grid, Right, Form, Footer, FooterTab, Drawer, Icon, Item, Input, Thumbnail, Col, Content, Tabs, Tab, TabHeading } from 'native-base';
import {SideBar} from './index';
import Modal from 'react-native-modal';
import ModalSelector from 'react-native-modal-selector';

class RegisterComponent extends Component {
    state = {
        isModalVisible: false,
    };
    toggleModal = () => {
        this.setState({ isModalVisible: !this.state.isModalVisible });
    };
    constructor(props){
        super(props);
        this.state={
          textInputValue: ''
        }
    }
    onValueChange(value: string) {
        this.setState({
            selected: value
        });
    }
    closeDrawer=()=>{
        this.drawer._root.close()
    }
    openDrawer=()=>{
        this.drawer._root.open()
    }
    render() {
        const {navigate}=this.props.navigation;
        let index = 0;
        const data = [
            { key: index++, label: 'تلفنی' },
            { key: index++, label: 'نوشتاری' },
            { key: index++, label: 'حضوری' }
        ];
        const data1 = [
            { key: index++, label: 'تستی۶' },
            { key: index++, label: 'تستی۷' },
            { key: index++, label: 'تستی۸' },
            { key: index++, label: 'تستی۹' },
            { key: index++, label: 'تستی۱۰' }
        ];
        const data2 = [
            { key: index++, label: 'تهران' },
            { key: index++, label: 'مشهد' },
            { key: index++, label: 'شیراز' },
            { key: index++, label: 'اصفهان' },
            { key: index++, label: 'تبریز' }
        ];
        const data3 = [
            { key: index++, label: 'مشاور خانواده' },
            { key: index++, label: 'مشاور ازدواج' },
            { key: index++, label: 'مشاور کاری' },
        ];
        const data4 = [
            { key: index++, label: 'تستی' },
            { key: index++, label: 'تستی۲' },
            { key: index++, label: 'تستی۳' },
            { key: index++, label: 'تستی۴' },
            { key: index++, label: 'تستی۵' }
        ];
        return (
            <Drawer
            ref={(ref)=>{this.drawer=ref;}}
            content={<SideBar navigate={navigate}/>}
            onClose={()=>this.closeDrawer()}
            >
                <Container>
                    <Header style={{backgroundColor:'#3f4392'}}>
                        <Left>
                            <Grid>
                                <Col style={{width:'20%'}}>
                                    <Button transparent onPress={()=>this.props.navigation.goBack()}>
                                        <Icon style={{color:'#FFF'}} name="ios-arrow-back" size={30} type="Ionicons"/>
                                    </Button>
                                </Col>
                                <Col style={{width:'50%'}}>
                                    <Button transparent title="Show modal" onPress={this.toggleModal} >
                                        <Icon name="search" type="FontAwesome" style={{color:'#FFF',fontSize:30}} />
                                    </Button>
                                    <Modal isVisible={this.state.isModalVisible}>
                                        <View style={{ flex: 1,paddingTop:'30%',}}>
                                            <View style={{backgroundColor:'#FFF',borderRadius:20}}>
                                                <Button
                                                    style={{
                                                        backgroundColor:'#3f4392',
                                                        borderRadius:20,
                                                        borderBottomLeftRadius:0,
                                                        borderBottomRightRadius:0,
                                                    }}
                                                    title="Hide modal" onPress={this.toggleModal} 
                                                >
                                                    <Icon style={{color:'#FFF',fontSize:25,marginLeft: 10,}} name="close" type="AntDesign"/>
                                                </Button>
                                                <Card style={{paddingBottom:'5%',paddingTop:'5%'}}>
                                                    <Thumbnail source={require('../img/hs.jpg')} style={{marginLeft:'43%',}}/>
                                                    <Text style={{textAlign:'center',fontWeight:'bold',marginTop:15}}>سید حسین سعیدی</Text>
                                                    <Text style={{textAlign:'center',marginTop:5}}>مشاور حقوقی</Text>
                                                </Card>
                                                <ModalSelector
                                                    data={data}
                                                    initValue="انتخاب نوع مشاوره"
                                                    style={[styles.ModalSelector]}
                                                />
                                                <ModalSelector
                                                    data={data2}
                                                    initValue="انتخاب شهر مشاور"
                                                    style={[styles.ModalSelector]}
                                                />
                                                <ModalSelector
                                                    data={data3}
                                                    initValue="انتخاب تخصص مشاور"
                                                    style={[styles.ModalSelector]}
                                                />
                                                <Button full style={{backgroundColor:'#3f4392',fontSize:15,margin:5,borderBottomLeftRadius:20,borderBottomEndRadius:20}}>
                                                    <Text>جستجو</Text>
                                                </Button>
                                            </View>  
                                        </View>
                                    </Modal>
                                </Col>
                            </Grid>
                        </Left>
                        <Right>
                            <Grid>
                                <Col style={{width:'75%'}}>
                                    <Button style={{backgroundColor:'transparent',}} onPress={()=>this.props.navigate('Home')} >
                                        <Image source={require('../img/topmo1.png')}/>
                                    </Button>
                                </Col>
                                <Col style={{width:'25%'}}>
                                    <Button transparent onPress={()=>this.openDrawer()}>
                                        <Icon name="navicon" type="FontAwesome" style={{color:'#FFF'}}/>
                                    </Button>
                                </Col>
                            </Grid>
                        </Right>
                    </Header>
                    <Content>
                        <View style={{borderBottomWidth:2,borderBottomColor:'#3f4392',marginTop:30,marginBottom:25}}></View>
                        <View style={{position:'absolute',marginLeft: '33%',}}>
                            <Text style={{textAlign:'center',backgroundColor:'#FFF',marginTop:18,color:'#3f4392',fontWeight:'bold',}}>ثبت نام</Text>
                        </View>
                        <Tabs initialPage={1}>
                            <Tab heading={
                                <TabHeading>
                                    <Text>ثبت نام کاربر</Text>
                                    <Icon name="user-alt" type="FontAwesome5" style={{fontSize:20}}/>
                                </TabHeading>
                            }>
                                <View style={{paddingVertical:20}}>
                                    <Form style={{paddingRight:'5%',paddingLeft:'2%'}}>
                                        <Item style={[styles.Item,{}]}>
                                            <Input placeholder="نام..." style={[styles.Input,{}]}/>
                                        </Item>
                                        <Item style={[styles.Item,{}]}>
                                            <Input placeholder="نام خانوادگی..." style={[styles.Input,{marginTop:5}]}/>
                                        </Item>
                                        <Item style={[styles.Item,{}]}>
                                            <Input placeholder="شماره همراه..." style={[styles.Input,{marginTop:5}]}/>
                                        </Item>
                                        <Item style={[styles.Item,{}]}>
                                            <Input placeholder="ایمیل..." style={[styles.Input,{marginTop:5}]}/>
                                        </Item>
                                        <Item style={[styles.Item,{}]}>
                                            <Input placeholder="رمز عبور..." style={[styles.Input,{marginTop:5}]}/>
                                        </Item>
                                        <Item style={[styles.Item,{}]}>
                                            <Input placeholder="تکرار رمز عبور..." style={[styles.Input,{marginTop:5}]}/>
                                        </Item>
                                    </Form>
                                    <Button full style={[styles.ButtonStyle,{}]}>
                                        <Text>ثبت نام</Text>
                                        <Icon name="login" type="AntDesign" />
                                    </Button>
                                </View>
                            </Tab>
                            <Tab heading={
                                <TabHeading>
                                    <Text>ثبت نام مشاور</Text>
                                    <Icon name="user-tie" type="FontAwesome5" style={{fontSize:20}}/>
                                </TabHeading>
                            }>
                                <View style={{paddingVertical:20}}>
                                    <Form style={{paddingRight:'5%',paddingLeft:'2%'}}>
                                        <Item style={[styles.Item,{}]}>
                                            <Input placeholder="نام..." style={[styles.Input,{}]} placeholderTextColor={'#3f4392'}/>
                                        </Item>
                                        <Item style={[styles.Item,{}]}>
                                            <Input placeholder="نام خانوادگی..." style={[styles.Input,{marginTop:5}]} placeholderTextColor={'#3f4392'}/>
                                        </Item>
                                        <Item style={[styles.Item,{}]}>
                                            <Input placeholder="شماره همراه..." style={[styles.Input,{marginTop:5}]} placeholderTextColor={'#3f4392'}/>
                                        </Item>
                                        <Item style={[styles.Item,{}]}>
                                            <Input placeholder="ایمیل..." style={[styles.Input,{marginTop:5}]} placeholderTextColor={'#3f4392'}/>
                                        </Item>
                                        <View style={{ justifyContent:'space-around', padding:0,marginLeft:15,marginTop:9}}>
                                            <ModalSelector
                                                data={data1}
                                                initValue="شغل"
                                            />
                                        </View>
                                        <View style={{padding:0,marginLeft:15,marginTop:9,borderRadius:20}}>
                                            <ModalSelector
                                                data={data4}
                                                initValue="تخصص"
                                            />
                                        </View>
                                        <Item style={[styles.Item,{}]}>
                                            <Input placeholder="سایر تخصص ها..." style={[styles.Input,{marginTop:5}]} placeholderTextColor={'#3f4392'}/>
                                        </Item>
                                        <Item style={[styles.Item,{}]}>
                                            <Input placeholder="رمز عبور..." style={[styles.Input,{marginTop:5}]} placeholderTextColor={'#3f4392'}/>
                                        </Item>
                                        <Item style={[styles.Item,{}]}>
                                            <Input placeholder="تکرار رمز عبور..." style={[styles.Input,{marginTop:5}]} placeholderTextColor={'#3f4392'}/>
                                        </Item>
                                    </Form>
                                    <Button full style={[styles.ButtonStyle,{}]}>
                                        <Text>ثبت نام</Text>
                                        <Icon name="login" type="AntDesign" />
                                    </Button>
                                </View>
                            </Tab>
                        </Tabs>
                    </Content>
                    <Footer style={styles.footerStyle}>
                        <FooterTab>
                            <Button vertical onPress={()=>this.props.navigation.navigate('Profile')}>
                                <Icon name="user" type="Entypo" style={{color:'#fff',fontSize:30}}/>
                                <Text style={styles.textStyle}>پروفایل</Text>
                            </Button>
                            <Button vertical onPress={()=>this.props.navigation.navigate('Consultantslist')}>
                                <Icon name="perm-contact-calendar" type="MaterialIcons" style={{color:'#fff',fontSize:30}}/>
                                <Text style={styles.textStyle}>مشاوران</Text>
                            </Button>
                            <Button vertical onPress={()=>this.props.navigation.navigate('Map')}>
                                <Icon name="map-marked-alt" type="FontAwesome5" style={{color:'#fff',fontSize:30}}/>
                                <Text style={styles.textStyle}>نقشه</Text>
                            </Button>
                            <Button vertical onPress={()=>this.props.navigation.navigate('Reports')}>
                                <Icon name="md-list-box" type="Ionicons" style={{color:'#fff',fontSize:30}}/>
                                <Text style={styles.textStyle}>گزارشات</Text>
                            </Button>
                            <Button vertical onPress={()=>this.props.navigation.navigate('Home')}>
                                <Icon name="home" type="FontAwesome" style={{color:'#fff',fontSize:30}}/>
                                <Text style={styles.textStyle}>خانه</Text>
                            </Button>
                        </FooterTab>
                    </Footer>
                </Container>
            </Drawer>
        );
    }
}

const styles = StyleSheet.create({
    footerStyle:{
        backgroundColor:'#3f4392',
        height:65,
    },
    textStyle:{
        color:'#FFF',
        marginTop:3,
        fontSize:13,
    },
    Input:{
        textAlign:'right',
        borderWidth: 1,
        borderColor: '#999',
        borderRadius:0,
        padding:15,
        borderWidth: 0,
        backgroundColor:'#ddd',
        color:'#3f4392',
        height:40,
        fontSize:13
    },
    Item:{
        borderBottomColor:'#fff',
    },
    ButtonStyle:{
        backgroundColor:'green',
        marginRight:17,
        marginVertical:6,
        borderRadius:15,
        marginLeft:21,
        borderTopLeftRadius:0 ,
        borderTopRightRadius:0 ,
        height:40
    }
})

export {RegisterComponent}
