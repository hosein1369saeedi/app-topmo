
import React, {Component} from 'react';
import {Image,StyleSheet} from 'react-native'
import {Container,Header, Left, Button,Text, Footer, Right, Content, Form, Drawer, Icon, Item, Input, FooterTab, View, Grid, Col, Thumbnail, Card} from 'native-base';
import {SideBar} from './index';
import Modal from 'react-native-modal';
import ModalSelector from 'react-native-modal-selector';

class MyDetailsComponent extends Component{
  state = {
    isModalVisible: false,
  }
  toggleModal = () => {
      this.setState({ isModalVisible: !this.state.isModalVisible });
  }
  constructor(props){
      super(props);
      this.state={
        textInputValue: ''
      }
  }
  closeDrawer=()=>{
    this.drawer._root.close()
  }
  openDrawer=()=>{
    this.drawer._root.open()
  }
  render() {
    let index = 0;
    const data = [
      { key: index++, label: 'تلفنی'},
      { key: index++, label: 'نوشتاری'},
      { key: index++, label: 'حضوری'}
    ];
    const data2 = [
      { key: index++, label: 'تهران'},
      { key: index++, label: 'مشهد'},
      { key: index++, label: 'شیراز'},
      { key: index++, label: 'اصفهان'},
      { key: index++, label: 'تبریز'}
    ];
    const data3 = [
      { key: index++, label: 'مشاور خانواده'},
      { key: index++, label: 'مشاور ازدواج'},
      { key: index++, label: 'مشاور کاری'},
    ];
    const {navigate}=this.props.navigation;
    return (
      <Drawer
        ref={(ref)=>{this.drawer=ref;}}
        content={<SideBar navigate={navigate}/>}
        onClose={()=>this.closeDrawer()}
      >
        <Container>
          <Header style={{backgroundColor:'#3f4392'}}>
            <Left>
              <Grid>
                <Col style={{width:'20%'}}>
                  <Button transparent onPress={()=>this.props.navigation.goBack()}>
                    <Icon style={{color:'#FFF'}} name="ios-arrow-back" size={30} type="Ionicons"/>
                  </Button>
                </Col>
                <Col style={{width:'50%'}}>
                  <Button transparent title="Show modal" onPress={this.toggleModal} >
                    <Icon name="search" type="FontAwesome" style={{color:'#FFF',fontSize:30}} />
                  </Button>
                  <Modal isVisible={this.state.isModalVisible}>
                    <View style={{ flex: 1,paddingTop:'30%',}}>
                      <View style={{backgroundColor:'#FFF',borderRadius:20}}>
                        <Button
                          style={{
                            backgroundColor:'#3f4392',
                            borderRadius:20,
                            borderBottomLeftRadius:0,
                            borderBottomRightRadius:0,
                            width:'100%'
                          }}
                          title="Hide modal" onPress={this.toggleModal} 
                        >
                          <Icon style={{color:'#FFF',fontSize:25,marginLeft: 10,}} name="close" type="AntDesign"/>
                        </Button>
                        <Card style={{paddingBottom:'5%',paddingTop:'5%'}}>
                          <Thumbnail source={require('../img/user.png')} style={{marginLeft:'43%',}}/>
                          <Text style={{textAlign:'center',fontWeight:'bold',marginTop:15}}>سید حسین سعیدی</Text>
                          <Text style={{textAlign:'center',marginTop:5}}>مشاور حقوقی</Text>
                        </Card>
                        <ModalSelector
                          data={data}
                          initValue="انتخاب نوع مشاوره"
                          style={[styles.ModalSelector]}
                        />
                        <ModalSelector
                          data={data2}
                          initValue="انتخاب شهر مشاور"
                          style={[styles.ModalSelector]}
                        />
                        <ModalSelector
                          data={data3}
                          initValue="انتخاب تخصص مشاور"
                          style={[styles.ModalSelector]}
                        />
                        <Button full style={{backgroundColor:'#3f4392',fontSize:15,margin:5,borderBottomLeftRadius:20,borderBottomEndRadius:20}}>
                          <Text>جستجو</Text>
                        </Button>
                      </View>  
                    </View>
                  </Modal>
                </Col>
              </Grid>
            </Left>
            <Right>
              <Grid>
                <Col style={{width:'75%'}}>
                  <Button style={{backgroundColor:'transparent',}} onPress={()=>this.props.navigate('Home')} >
                    <Image source={require('../img/topmo1.png')}/>
                  </Button>
                </Col>
                <Col style={{width:'25%'}}>
                  <Button transparent onPress={()=>this.openDrawer()}>
                    <Icon name="navicon" type="FontAwesome" style={{color:'#FFF'}}/>
                  </Button>
                </Col>
              </Grid>
            </Right>
          </Header>
          <Content>
            <View style={{borderBottomWidth:2,borderBottomColor:'#3f4392',marginTop:30,marginBottom:30}}></View>
            <View style={{position:'absolute',marginLeft: '30%',}}>
              <Text style={{textAlign:'center',backgroundColor:'#FFF',marginTop:18,color:'#3f4392',fontWeight:'bold',}}>ویرایش اطلاعات</Text>
            </View>
            <View>
              <Grid>
                <Col style={{width:'80%'}}>
                  <Form style={{paddingRight:15,}}>
                    <Item>
                      <Input placeholder="نام..." style={{textAlign:'right'}}/>
                    </Item>
                    <Item>
                      <Input placeholder="نام خانوادگی..." style={{textAlign:'right'}}/>
                    </Item>
                  </Form>
                </Col>
                <Col style={{width:'20%'}}>
                  <Thumbnail source={require('../img/user.png')} style={{marginTop:14}}/>
                  <Button transparent >
                    <Icon style={{color:'#999',}} name="images" type="Entypo" />
                  </Button>
                </Col>
              </Grid>
            </View>
            <Form>
              <Item>
                <Input placeholder="جیمیل..." style={{textAlign:'right',margin:5}}/>
                <Icon active name="email" type="MaterialCommunityIcons" style={{color:'#999'}} />
              </Item>
            </Form>
            <Form>
              <Item>
                <Input placeholder="شماره همراه..." style={{textAlign:'right',margin:5}}/>
                <Icon active name="screen-smartphone" type="SimpleLineIcons" style={{color:'#999'}} />
              </Item>
            </Form>
            <Form>
              <Item>
                <Input placeholder="آی دی تلگرام..." style={{textAlign:'right',margin:5}}/>
                <Icon active name="telegram" type="FontAwesome" style={{color:'#999'}} />
              </Item>
            </Form>
            <Form>
              <Item>
                <Input placeholder="عنوان شغلی..." style={{textAlign:'right',margin:5}}/>
                <Icon active name="users-cog" type="FontAwesome5" style={{color:'#999'}} />
              </Item>
            </Form>
            <Grid style={{direction:'rtl'}}>
              <Col>
                <Button block style={[styles.ButtonStyle,{backgroundColor:'#3F4392'}]} onPress={()=>this.props.navigation.navigate('ResetPassword')}>
                  <Text style={{fontSize:11,fontWeight:'bold'}}>تغییر رمز عبور</Text>
                </Button>
              </Col>
              <Col>
                <Button block style={[styles.ButtonStyle,{backgroundColor:'red'}]}>
                  <Text style={{fontSize:11,fontWeight:'bold'}}>انصراف</Text>
                </Button>
              </Col>
              <Col>
                <Button block style={[styles.ButtonStyle,{backgroundColor:'green'}]}>
                  <Text style={{fontSize:11,fontWeight:'bold'}}>ذخیره مشخصات</Text>
                </Button>
              </Col>
            </Grid>
          </Content>
          <Footer style={styles.footerStyle}>
            <FooterTab>
              <Button vertical onPress={()=>this.props.navigation.navigate('Profile')}>
                <Icon name="user" type="Entypo" style={{color:'#fff',fontSize:30}}/>
                <Text style={styles.textStyle}>پروفایل</Text>
              </Button>
              <Button vertical onPress={()=>this.props.navigation.navigate('Consultantslist')}>
                <Icon name="perm-contact-calendar" type="MaterialIcons" style={{color:'#fff',fontSize:30}}/>
                <Text style={styles.textStyle}>مشاوران</Text>
              </Button>
              <Button vertical onPress={()=>this.props.navigation.navigate('Map')}>
                <Icon name="map-marked-alt" type="FontAwesome5" style={{color:'#fff',fontSize:30}}/>
                <Text style={styles.textStyle}>نقشه</Text>
              </Button>
              <Button vertical onPress={()=>this.props.navigation.navigate('Reports')}>
                <Icon name="md-list-box" type="Ionicons" style={{color:'#fff',fontSize:30}}/>
                <Text style={styles.textStyle}>گزارشات</Text>
              </Button>
              <Button vertical onPress={()=>this.props.navigation.navigate('Home')}>
                <Icon name="home" type="FontAwesome" style={{color:'#fff',fontSize:30}}/>
                <Text style={styles.textStyle}>خانه</Text>
              </Button>
            </FooterTab>
          </Footer>
        </Container>
      </Drawer>
    );
  }
}
const styles = StyleSheet.create({
  footerStyle:{
    backgroundColor:'#3f4392',
    height:65,
  },
  textStyle:{
    color:'#FFF',
    marginTop:3,
    fontSize:13,
  },
  ButtonStyle:{
    margin:5,
    borderRadius:10,
    height:30
  },
})
export {MyDetailsComponent}
