
import React, {Component} from 'react';
import {Image,View,StyleSheet} from 'react-native';
import {Container, Header, Left, Button, Text, Body, Right, Content, Footer, FooterTab, Drawer, Icon, CardItem, Grid, Col, Card, Thumbnail}from 'native-base';
import {SideBar} from './index';
import ModalSelector from 'react-native-modal-selector';
import Modal from 'react-native-modal';
import { Badge } from 'react-native-elements';

class Home extends  Component{
  state = {
    isModalVisible: false
  };
  toggleModal = () => {
    this.setState({ isModalVisible: !this.state.isModalVisible });
  };
  constructor(props){
    super(props);
    this.state={
      textInputValue: ''
    }
  }
  closeDrawer=()=>{
    this.drawer._root.close()
  }
  openDrawer=()=>{
    this.drawer._root.open()
  }
  render() {
    let index = 0;
    const data = [
      { key: index++, label: 'تلفنی'},
      { key: index++, label: 'نوشتاری'},
      { key: index++, label: 'حضوری'}
    ];
    const data2 = [
      { key: index++, label: 'تهران'},
      { key: index++, label: 'مشهد'},
      { key: index++, label: 'شیراز'},
      { key: index++, label: 'اصفهان'},
      { key: index++, label: 'تبریز'}
    ];
    const data3 = [
      { key: index++, label: 'مشاور خانواده'},
      { key: index++, label: 'مشاور ازدواج'},
      { key: index++, label: 'مشاور کاری'},
    ];
    const {navigate}=this.props.navigation;
    return (
      <Drawer
        ref={(ref)=>{this.drawer=ref;}}
        content={<SideBar navigate={navigate}/>}
        onClose={()=>this.closeDrawer()}
      >
        <Container style={{backgroundColor:'#DDD'}}>
          <Header searchBar rounded style={{backgroundColor:'#3f4392',height:200,position:'relative',}}>
            <Left style={{marginTop:-140,}}>
              <Button transparent title="Show modal" onPress={this.toggleModal} >
                <Icon name="ios-search" type="Ionicons" style={{color:'#FFF',fontSize:30}} />
              </Button>
              <Modal isVisible={this.state.isModalVisible}>
                <View style={{ flex: 1,paddingTop:'30%',}}>
                  <View style={{backgroundColor:'#FFF',borderRadius:20}}>
                    <Button
                      style={{
                        backgroundColor:'#3f4392',
                        borderRadius:20,
                        borderBottomLeftRadius:0,
                        borderBottomRightRadius:0,
                      }}
                      title="Hide modal" onPress={this.toggleModal} 
                    >
                      <Icon style={{color:'#FFF',fontSize:25,marginLeft: 10,}} name="close" type="AntDesign"/>
                    </Button>
                    <Card style={{paddingBottom:'5%',paddingTop:'5%'}}>
                      <Thumbnail source={require('../img/hs.jpg')} style={{marginLeft:'43%',}}/>
                      <Text style={{textAlign:'center',fontWeight:'bold',marginTop:15}}>سید حسین سعیدی</Text>
                      <Text style={{textAlign:'center',marginTop:5}}>مشاور حقوقی</Text>
                    </Card>
                    <ModalSelector
                      data={data}
                      initValue="انتخاب نوع مشاوره"
                      style={[styles.ModalSelector]}
                    />
                    <ModalSelector
                      data={data2}
                      initValue="انتخاب شهر مشاور"
                      style={[styles.ModalSelector]}
                    />
                    <ModalSelector
                      data={data3}
                      initValue="انتخاب تخصص مشاور"
                      style={[styles.ModalSelector]}
                    />
                    <Button full style={{backgroundColor:'#3f4392',fontSize:15,margin:5,borderBottomLeftRadius:20,borderBottomEndRadius:20}}>
                      <Text>جستجو</Text>
                    </Button>
                  </View>  
                </View>
              </Modal>
            </Left>
            <Body>
              <View style={[styles.shadowStyle,{}]}>
                <Image source={require('../img/topmo.png')}/>
              </View>
            </Body>
            <Badge value="۱" status="error" containerStyle={{ position: 'absolute', top: 45, right: 30,zIndex:1}}/>
            <Right style={{marginTop:-140,}}>
              <Button transparent onPress={()=>this.openDrawer()}>
                <Icon name="navicon" type="FontAwesome" style={{color:'#FFF'}}/>
              </Button>
            </Right>
          </Header>
          <Content style={styles.contentStyle}>
            <CardItem style={[styles.CardItemStyle,{marginTop:'30%'}]}>
              <Grid>
                <Col style={{width:'80%'}}>
                  <Button style={[styles.buttonStyle,{marginTop:60}]} onPress={()=>this.props.navigation.navigate('Consultantslist')}>
                    <Text style={[styles.buyStyle,{marginLeft:-10}]}>مشاوره میخواهم</Text>
                  </Button>
                </Col>
                <Col style={{width:'20%',borderRadius:10,}}>
                  <View >
                    <Image style={[styles.ImageStyle,{marginTop:53,}]} source={require('../img/conversation1.png')}/>
                  </View>
                </Col>
              </Grid>
            </CardItem>
            <CardItem style={[styles.CardItemStyle,{}]}>
              <Grid>
                <Col style={{width:'80%'}}>
                  <Button style={styles.buttonStyle} onPress={()=>this.props.navigation.navigate('')}>
                    <Text style={[styles.buyStyle]}>مشاوره میدهم</Text>
                  </Button>
                </Col>
                <Col style={{width:'20%',borderRadius:10,}}>
                  <View >
                    <Image style={[styles.ImageStyle,{}]} source={require('../img/conversation.png')}/>
                  </View>
                </Col>
              </Grid>
            </CardItem>
            <CardItem style={[styles.CardItemStyle,{}]}>
              <Grid>
                <Col style={{width:'80%'}}>
                  <Button style={styles.buttonStyle} onPress={()=>this.props.navigation.navigate('Services')}>
                    <Text style={[styles.buyStyle,{marginLeft:-5}]}>خدمات مشاوره</Text>
                  </Button>
                </Col>
                <Col style={{width:'20%',borderRadius:10,}}>
                  <View >
                    <Image style={[styles.ImageStyle,{}]} source={require('../img/support.png')}/>
                  </View>
                </Col>
              </Grid>
            </CardItem>
            <CardItem style={[styles.CardItemStyle,{}]}>
              <Grid>
                <Col style={{width:'80%'}}>
                  <Button style={styles.buttonStyle} onPress={()=>this.props.navigation.navigate('')}>
                    <Text style={[styles.buyStyle,{marginLeft:-5}]}>قوانین و مقررات</Text>
                  </Button>
                </Col>
                <Col style={{width:'20%',borderRadius:10,}}>
                  <View  style={{}}>
                    <Image style={[styles.ImageStyle,{}]} source={require('../img/lawbook.png')}/>
                  </View>
                </Col>
              </Grid>
            </CardItem>
            <CardItem style={[styles.CardItemStyle,{}]}>
              <Grid>
                <Col style={{width:'80%'}}>
                  <Button style={styles.buttonStyle}>
                    <Text style={[styles.buyStyle,{marginLeft:-6}]}>راهنمای اپلیکیشن</Text>
                  </Button>
                </Col>
                <Col style={{width:'20%',borderRadius:10,}}>
                  <View>
                    <Image style={[styles.ImageStyle,{}]} source={require('../img/apphelp.png')}/>
                  </View>
                </Col>
              </Grid>
            </CardItem>
          </Content>  
          <Footer style={styles.footerStyle}>
            <FooterTab>
              <Button vertical onPress={()=>this.props.navigation.navigate('Profile')}>
                <Icon name="user" type="Entypo" style={{color:'#fff',fontSize:30}}/>
                <Text style={styles.textStyle}>پروفایل</Text>
              </Button>
              <Button vertical onPress={()=>this.props.navigation.navigate('Consultantslist')}>
                <Icon name="perm-contact-calendar" type="MaterialIcons" style={{color:'#fff',fontSize:30}}/>
                <Text style={styles.textStyle}>مشاوران</Text>
              </Button>
              <Button vertical onPress={()=>this.props.navigation.navigate('Map')}>
                <Icon name="map-marked-alt" type="FontAwesome5" style={{color:'#fff',fontSize:30}}/>
                <Text style={styles.textStyle}>نقشه</Text>
              </Button>
              <Button vertical onPress={()=>this.props.navigation.navigate('Reports')}>
                <Icon name="md-list-box" type="Ionicons" style={{color:'#fff',fontSize:30}}/>
                <Text style={styles.textStyle}>گزارشات</Text>
              </Button>
              <Button vertical onPress={()=>this.props.navigation.navigate('Home')}>
                <Icon name="home" type="FontAwesome" style={{color:'#fff',fontSize:30}}/>
                <Text style={styles.textStyle}>خانه</Text>
              </Button>
            </FooterTab>
          </Footer>
        </Container>
      </Drawer>
    );
  }
}
const styles = StyleSheet.create({
  footerStyle:{
    backgroundColor:'#3f4392',
    height:65,
  },
  textStyle:{
    color:'#FFF',
    marginTop:3,
    fontSize:13,
  },
  contentStyle:{
    position:'relative',
    zIndex:-1,
  },
  buttonStyle:{
    width:"100%",
    height:50,
    padding:0,
    marginTop:6,
    backgroundColor:'#FFF',
    textAlign:'center',
    paddingLeft:'20%',
  },
  buyStyle:{
    color:'#3f4392',
    fontSize:25,
    textAlign:'center',
  },
  CardItemStyle:{
    height:80,
    margin:5,
    marginTop:'1%',
    borderRadius:15,
    borderColor: '#3f4392',
    borderBottomWidth: 0,
    shadowColor: '#3f4392',
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.4,
    shadowRadius: 5,
    elevation: 4,
  },
  shadowStyle:{
    backgroundColor:'rgba(255, 255, 255, 0.7)',
    padding:'40% 0%',
    borderRadius:25,
    marginTop:'130%',
    height:200,
    borderColor: '#3f4392',
    borderBottomWidth: 0,
    shadowColor: '#3f4392',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.5,
    shadowRadius: 10,
    elevation: 1,
    marginLeft: 9,
    marginRight: 5,
  },
  ModalSelector:{
    margin:5,
    padding:5,
  },
  ImageStyle:{
    borderColor:'#3f4392',
    borderWidth:1,
    marginLeft:10,
    marginTop:0,
    borderRadius:10,
  },
})
export {Home}
