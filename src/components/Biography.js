import React, {Component} from 'react';
import {View,StyleSheet,Image} from 'react-native';
import {Container,Header, Left, Button,Text, Grid, Col, Right, Content, Drawer, Icon, FooterTab, Footer, Form, Textarea, Card, Thumbnail} from 'native-base';
import {SideBar} from '.';
import ModalSelector from 'react-native-modal-selector';
import Modal from 'react-native-modal'

class BiographyComponent extends Component {
    state = {
        isModalVisible: false,
    }
    toggleModal = () => {
        this.setState({ isModalVisible: !this.state.isModalVisible });
    }
    constructor(props){
        super(props);
        this.state={
            selected: "key0",
        }
    }
    onValueChange(value: string) {
        this.setState({
            selected: value
        });
    }
    closeDrawer=()=>{
        this.drawer._root.close()
    }
    openDrawer=()=>{
        this.drawer._root.open()
    }
    render() {
        let index = 0;
        const data = [
            { key: index++, label: 'تلفنی'},
            { key: index++, label: 'نوشتاری'},
            { key: index++, label: 'حضوری'}
        ];
        const data2 = [
            { key: index++, label: 'تهران'},
            { key: index++, label: 'مشهد'},
            { key: index++, label: 'شیراز'},
            { key: index++, label: 'اصفهان'},
            { key: index++, label: 'تبریز'}
        ];
        const data3 = [
            { key: index++, label: 'مشاور خانواده'},
            { key: index++, label: 'مشاور ازدواج'},
            { key: index++, label: 'مشاور کاری'},
        ];
        const {navigate}=this.props.navigation;
        return (
            <Drawer
                ref={(ref)=>{this.drawer=ref;}}
                content={<SideBar navigate={navigate}/>}
                onClose={()=>this.closeDrawer()}
            >
                <Container>
                    <Header style={{backgroundColor:'#3f4392'}}>
                        <Left>
                            <Grid>
                                <Col style={{width:'20%'}}>
                                    <Button transparent onPress={()=>this.props.navigation.goBack()}>
                                        <Icon style={{color:'#FFF'}} name="ios-arrow-back" size={30} type="Ionicons"/>
                                    </Button>
                                </Col>
                                <Col style={{width:'50%'}}>
                                    <Button transparent title="Show modal" onPress={this.toggleModal} >
                                        <Icon name="ios-search" type="Ionicons" style={{color:'#FFF',fontSize:30}} />
                                    </Button>
                                    <Modal isVisible={this.state.isModalVisible}>
                                        <View style={{ flex: 1,paddingTop:'30%',}}>
                                            <View style={{backgroundColor:'#FFF',borderRadius:20}}>
                                                <Button
                                                    style={{
                                                        backgroundColor:'#3f4392',
                                                        borderRadius:20,
                                                        borderBottomLeftRadius:0,
                                                        borderBottomRightRadius:0,
                                                    }}
                                                    title="Hide modal" onPress={this.toggleModal} 
                                                >
                                                    <Icon style={{color:'#FFF',fontSize:25,marginLeft: 10,}} name="close" type="AntDesign"/>
                                                </Button>
                                                <Card style={{paddingBottom:'5%',paddingTop:'5%'}}>
                                                    <Thumbnail source={require('../img/hs.jpg')} style={{marginLeft:'43%',}}/>
                                                    <Text style={{textAlign:'center',fontWeight:'bold',marginTop:15}}>سید حسین سعیدی</Text>
                                                    <Text style={{textAlign:'center',marginTop:5}}>مشاور حقوقی</Text>
                                                </Card>
                                                <ModalSelector
                                                    data={data}
                                                    initValue="انتخاب نوع مشاوره"
                                                    stylقe={[styles.ModalSelector]}
                                                />
                                                <ModalSelector
                                                    data={data2}
                                                    initValue="انتخاب شهر مشاور"
                                                    style={[styles.ModalSelector]}
                                                />
                                                <ModalSelector
                                                    data={data3}
                                                    initValue="انتخاب تخصص مشاور"
                                                    style={[styles.ModalSelector]}
                                                />
                                                <Button full style={{backgroundColor:'#3f4392',fontSize:15,margin:5,borderBottomLeftRadius:20,borderBottomEndRadius:20}}>
                                                    <Text>جستجو</Text>
                                                </Button>
                                            </View>  
                                        </View>
                                    </Modal>
                                </Col>
                            </Grid>
                        </Left>
                        <Right>
                            <Grid>
                                <Col style={{width:'75%'}}>
                                    <Button style={{backgroundColor:'transparent',}} onPress={()=>this.props.navigate('Home')} >
                                        <Image source={require('../img/topmo1.png')}/>
                                    </Button>
                                </Col>
                                <Col style={{width:'25%'}}>
                                    <Button transparent onPress={()=>this.openDrawer()}>
                                        <Icon name="navicon" type="FontAwesome" style={{color:'#FFF'}}/>
                                    </Button>
                                </Col>
                            </Grid>
                        </Right>
                    </Header>
                    <Content>
                        <View style={{borderBottomWidth:2,borderBottomColor:'#3f4392',marginTop:30,marginBottom:25}}></View>
                        <View style={{position:'absolute',marginLeft: '33%',}}>
                            <Text style={{textAlign:'center',backgroundColor:'#FFF',marginTop:18,color:'#3f4392',fontWeight:'bold',}}>بیوگرافی</Text>
                        </View>
                        <Card style={{padding:15}}>
                            <Thumbnail style={{marginLeft:'39%',}} large source={require('../img/hs.jpg')} />
                            <Text style={{textAlign:'center',fontWeight:'bold'}}>سید حسین سعیدی</Text>
                            <Text note style={{textAlign:'center',}}>مشاور خانواده</Text>
                        </Card>
                        <View>
                            <Card style={{padding:10,margin:10}}>
                                <Text note style={{textAlign:'right'}}>بیوگرافی</Text>
                                <Text style={{textAlign:'right'}}>طراح وب سایت</Text>
                                <Text style={{textAlign:'right'}}>طراح وب سایت طراح اپلیکیشن</Text>
                            </Card>
                        </View>
                        <View>
                            <Card>
                                <Form>
                                    <Text style={{textAlign:'right',fontWeight:'bold',color:'#3f4392',margin:12}}>اضافه کردن به بیوگرافی :</Text>
                                    <Textarea style={styles.Textarea} rowSpan={5} bordered placeholder="نوشتن" />
                                    <Grid style={{margin:10}}>
                                        <Col>
                                            <Button full style={[styles.ButtonStyle,{backgroundColor:'green'}]}>
                                                <Text style={{fontWeight:'bold'}}>ذخیره</Text>
                                                <Icon name="checkcircle" type="AntDesign"/>
                                            </Button>
                                        </Col>
                                        <Col>
                                            <Button full style={[styles.ButtonStyle,{backgroundColor:'red'}]}>
                                                <Text style={{fontWeight:'bold'}}>انصراف</Text>
                                                <Icon name="closecircle" type="AntDesign"/>
                                            </Button>
                                        </Col>
                                    </Grid>
                                </Form>
                            </Card>
                        </View>
                    </Content>
                    <Footer style={styles.footerStyle}>
                        <FooterTab>
                            <Button vertical onPress={()=>this.props.navigation.navigate('Profile')}>
                                <Icon name="user" type="Entypo" style={{color:'#fff',fontSize:30}}/>
                                <Text style={styles.textStyle}>پروفایل</Text>
                            </Button>
                            <Button vertical onPress={()=>this.props.navigation.navigate('Consultantslist')}>
                                <Icon name="perm-contact-calendar" type="MaterialIcons" style={{color:'#fff',fontSize:30}}/>
                                <Text style={styles.textStyle}>مشاوران</Text>
                            </Button>
                            <Button vertical onPress={()=>this.props.navigation.navigate('Map')}>
                                <Icon name="map-marked-alt" type="FontAwesome5" style={{color:'#fff',fontSize:30}}/>
                                <Text style={styles.textStyle}>نقشه</Text>
                            </Button>
                            <Button vertical onPress={()=>this.props.navigation.navigate('Reports')}>
                                <Icon name="md-list-box" type="Ionicons" style={{color:'#fff',fontSize:30}}/>
                                <Text style={styles.textStyle}>گزارشات</Text>
                            </Button>
                            <Button vertical onPress={()=>this.props.navigation.navigate('Home')}>
                                <Icon name="home" type="FontAwesome" style={{color:'#fff',fontSize:30}}/>
                                <Text style={styles.textStyle}>خانه</Text>
                            </Button>
                        </FooterTab>
                    </Footer>
                </Container>
            </Drawer>
        );
    }
}
const styles = StyleSheet.create({
    footerStyle:{
      backgroundColor:'#3f4392',
      height:65,
    },
    textStyle:{
      color:'#FFF',
      marginTop:3,
      fontSize:13,
    },
    Textarea:{
        textAlign:'right',
        margin:10,
        borderRadius:10,
    },
    ButtonStyle:{
        borderRadius:10,
        margin:3,
        height:40
    },
    ModalSelector:{
        margin: 8,
        marginTop:10,
        marginBottom:2,
        backgroundColor:'#61d6cd',
        borderRadius:7,
        borderColor:'transparent'
    }
})
export {BiographyComponent};