
import React, {Component} from 'react';
import {TouchableOpacity,View,StyleSheet} from 'react-native';
import {Icon as Icn} from 'react-native-vector-icons/FontAwesome';
import {Picker,Header, Left, Button,Text, Body, Title, Right,
  Content, Footer, FooterTab, Drawer, Icon, Item, Input, DeckSwiper,
  CardItem, Card, Thumbnail, Grid, Col, Row} from 'native-base';
import {SideBar} from './index';
import Modal from 'react-native-modal';

class SearchModalComponent extends Component {
    state = {
        isModalVisible: false,
    };
    
    _toggleModal = () =>
    this.setState({ isModalVisible: !this.state.isModalVisible });
    
    constructor(props){
        super(props);
        this.state={
        selected: "key0",
        }
    }
    onValueChange(value: string) {
        this.setState({
        selected: value
        });
    }
    render() {
        return (
            <View>
                <TouchableOpacity style={{marginTop:10,marginLeft:10}} onPress={this._toggleModal}>
                    <Icon name="ios-search" style={{color:'#FFF'}} />
                </TouchableOpacity>
                <Modal isVisible={this.state.isModalVisible} style={{backgroundColor:'#FFF',borderRadius:20,flex:1}}>
                    <View style={{ flex: 1 }}>
                        <Header style={{paddingBottom:10,backgroundColor:'#3f4392',borderTopLeftRadius:20,borderTopRightRadius:20}}>
                            <Left>
                            <TouchableOpacity onPress={this._toggleModal}>
                                <Icon style={{color:'#FFF',fontSize:25,marginLeft: 10,}} name="close" type="AntDesign"/>
                            </TouchableOpacity>
                            </Left>
                            <Right></Right>
                        </Header>
                        <View>
                            <Card style={{paddingBottom:'5%',paddingTop:'5%'}}>
                                <Thumbnail source={require('../img/hs.jpg')} style={{marginLeft:'43%',}}/>
                                <Text style={{textAlign:'center',fontWeight:'bold',marginTop:15}}>سید حسین سعیدی</Text>
                                <Text style={{textAlign:'center',marginTop:5}}>مشاور حقوقی</Text>
                            </Card>
                            <Text style={{textAlign:'right',color:'#3f4392',paddingRight: 15,}}>انتخاب نوع مشاوره :</Text>
                            <Picker
                                mode="dropdown"
                                iosHeader="نوع مشاوره"
                                iosIcon={<Icon name="arrow-down" />}
                                style={{ width:'100%',borderBottomWidth:1,borderBottomColor:'#999',textAlign:'right'}}
                                selectedValue={this.state.selected}
                                onValueChange={this.onValueChange.bind(this)}
                            >
                                <Picker.Item label="انتخاب" value="key0" />
                                <Picker.Item label="حضوری" value="key1" />
                                <Picker.Item label="تلفنی" value="key2" />
                                <Picker.Item label="نوشتاری" value="key3" />
                            </Picker>
                            <Text style={{textAlign:'right',color:'#3f4392',paddingRight: 15,}}>انتخاب شهر :</Text>
                            <Picker
                                mode="dropdown"
                                iosHeader="نوع مشاوره"
                                iosIcon={<Icon name="arrow-down" />}
                                style={{ width:'100%',borderBottomWidth:1,borderBottomColor:'#999',textAlign:'right'}}
                                selectedValue={this.state.selected}
                                onValueChange={this.onValueChange.bind(this)}
                            >
                                <Picker.Item label="انتخاب" value="key0" />
                                <Picker.Item label="حضوری" value="key1" />
                                <Picker.Item label="تلفنی" value="key2" />
                                <Picker.Item label="نوشتاری" value="key3" />
                            </Picker>
                            <Text style={{textAlign:'right',color:'#3f4392',paddingRight: 15,}}>انتخاب تخصص مشاور :</Text>
                            <Picker
                                mode="dropdown"
                                iosHeader="نوع مشاوره"
                                iosIcon={<Icon name="arrow-down" />}
                                style={{ width:'100%',borderBottomWidth:1,borderBottomColor:'#999',textAlign:'right'}}
                                selectedValue={this.state.selected}
                                onValueChange={this.onValueChange.bind(this)}
                            >
                                <Picker.Item label="انتخاب" value="key0" />
                                <Picker.Item label="حضوری" value="key1" />
                                <Picker.Item label="تلفنی" value="key2" />
                                <Picker.Item label="نوشتاری" value="key3" />
                            </Picker>
                            <Button full style={{backgroundColor:'#3f4392',fontSize:15,margin:5,borderRadius:10}}>
                                <Text>جستجو</Text>
                            </Button>
                        </View>                            
                    </View>
                </Modal>
            </View>
        );
    }
}
const styles = StyleSheet.create({
  cardItemStyle:{
    marginTop:105,
    backgroundColor:'transparent',
  },
  textStyle:{
    textAlign:'center',
    marginTop:3,
  },
  texStyle:{
    textAlign:'center',
    color:'red',
    marginTop:5,
    marginBottom:5,
  },
  colStyle:{
    padding:3,
    backgroundColor:'#FFF',
    borderRadius:0,
    margin:5,
  },
  imageStyle:{
    width:'100%',
    height:100,
    borderRadius:0,
  },
  buttonStyle:{
    width:"100%",
    height:30,
    padding:0,
    margin:0,
    backgroundColor:'#C65390',
  },
  buyStyle:{
    color:'#FFF',
  }
})
export {SearchModalComponent}