import React, {Component} from 'react';
import {StyleSheet,Image,View} from 'react-native';
import {Container,Header, Left, Button,Text, Card, Grid, Right, Footer, FooterTab, Drawer, Icon, Thumbnail, Col, Content } from 'native-base';
import {SideBar} from './index';
import ModalSelector from 'react-native-modal-selector';
import Modal from 'react-native-modal';

class AboutusComponent extends Component {
    state = {
        isModalVisible: false,
    }
    toggleModal = () => {
        this.setState({ isModalVisible: !this.state.isModalVisible });
    }
    constructor(props){
        super(props);
        this.state={
            selected: "key0",
            textInputValue: ''
        }
    }
    onValueChange(value: string) {
        this.setState({
            selected: value
        });
    }
    closeDrawer=()=>{
        this.drawer._root.close()
    }
    openDrawer=()=>{
        this.drawer._root.open()
    }
    render() {
        let index = 0;
        const data = [
            { key: index++, label: 'تلفنی'},
            { key: index++, label: 'نوشتاری'},
            { key: index++, label: 'حضوری'}
        ];
        const data2 = [
            { key: index++, label: 'تهران'},
            { key: index++, label: 'مشهد'},
            { key: index++, label: 'شیراز'},
            { key: index++, label: 'اصفهان'},
            { key: index++, label: 'تبریز'}
        ];
        const data3 = [
            { key: index++, label: 'مشاور خانواده'},
            { key: index++, label: 'مشاور ازدواج'},
            { key: index++, label: 'مشاور کاری'},
        ];
        const {navigate}=this.props.navigation;
        return (
            <Drawer
                ref={(ref)=>{this.drawer=ref;}}
                content={<SideBar navigate={navigate}/>}
                onClose={()=>this.closeDrawer()}
            >
                <Container>
                    <Header style={{backgroundColor:'#3f4392'}}>
                        <Left>
                            <Grid>
                                <Col style={{width:'20%'}}>
                                    <Button transparent onPress={()=>this.props.navigation.goBack()}>
                                        <Icon style={{color:'#FFF'}} name="ios-arrow-back" size={30} type="Ionicons" />
                                    </Button>
                                </Col>
                                <Col style={{width:'50%'}}>
                                    <Button transparent title="Show modal" onPress={this.toggleModal} >
                                        <Icon name="ios-search" type="Ionicons" style={{color:'#FFF',fontSize:30}} />
                                    </Button>
                                    <Modal isVisible={this.state.isModalVisible}>
                                        <View style={{ flex: 1,paddingTop:'30%',}}>
                                            <View style={{backgroundColor:'#FFF',borderRadius:20}}>
                                                <Button
                                                    style={{
                                                        backgroundColor:'#3f4392',
                                                        borderRadius:20,
                                                        borderBottomLeftRadius:0,
                                                        borderBottomRightRadius:0,
                                                    }}
                                                    title="Hide modal" onPress={this.toggleModal} 
                                                >
                                                    <Icon style={{color:'#FFF',fontSize:25,marginLeft: 10,}} name="close" type="AntDesign"/>
                                                </Button>
                                                <Card style={{paddingBottom:'5%',paddingTop:'5%'}}>
                                                    <Thumbnail source={require('../img/hs.jpg')} style={{marginLeft:'43%',}}/>
                                                    <Text style={{textAlign:'center',fontWeight:'bold',marginTop:15}}>سید حسین سعیدی</Text>
                                                    <Text style={{textAlign:'center',marginTop:5}}>مشاور حقوقی</Text>
                                                </Card>
                                                <ModalSelector
                                                    data={data}
                                                    initValue="انتخاب نوع مشاوره"
                                                    stylقe={[styles.ModalSelector]}
                                                />
                                                <ModalSelector
                                                    data={data2}
                                                    initValue="انتخاب شهر مشاور"
                                                    style={[styles.ModalSelector]}
                                                />
                                                <ModalSelector
                                                    data={data3}
                                                    initValue="انتخاب تخصص مشاور"
                                                    style={[styles.ModalSelector]}
                                                />
                                                <Button full style={{backgroundColor:'#3f4392',fontSize:15,margin:5,borderBottomLeftRadius:20,borderBottomEndRadius:20}}>
                                                    <Text>جستجو</Text>
                                                </Button>
                                            </View>  
                                        </View>
                                    </Modal>
                                </Col>
                            </Grid>
                        </Left>
                        <Right>
                            <Grid>
                                <Col style={{width:'75%'}}>
                                    <Button style={{backgroundColor:'transparent',}} onPress={()=>this.props.navigate('Home')} >
                                        <Image source={require('../img/topmo1.png')}/>
                                    </Button>
                                </Col>
                                <Col style={{width:'25%'}}>
                                    <Button transparent onPress={()=>this.openDrawer()}>
                                        <Icon name="navicon" type="FontAwesome" style={{color:'#FFF'}}/>
                                    </Button>
                                </Col>
                            </Grid>
                        </Right>
                    </Header>
                    <Content>
                        <View style={{borderBottomWidth:2,borderBottomColor:'#3f4392',marginTop:30,marginBottom:25}}></View>
                        <View style={{position:'absolute',marginLeft: '33%',}}>
                            <Text style={{textAlign:'center',backgroundColor:'#FFF',marginTop:18,color:'#3f4392',fontWeight:'bold',}}>درباره تاپمو</Text>
                        </View>
                        <View>
                            <Card style={{padding:20}}>
                                <Text style={[styles.Cardtext,{color:'#3f4392'}]}> 
                                    امروزه زندگی ما با دنیای مجازی گره خورده است و با پیشرفت علم و فناوری هر روز بیش از پیش به کاربرد تکنولوژی و ارتباطات از راه دور آگاه می شویم و خود را نیازمند استفاده از امکانات پیش رو می بینیم. بر همین اساس ما بر آن شدیم تا با اغتنام فرصت و بهره گیری از زمینه ها و امکانات وسیع موجود، متخصصان و مشاوران را در زمینه های مختلف روانشناسی، کسب و کارهای اینترنتی و غیره، در محیطی امن و کارآمد در فضای مجازی گرد هم آوریم و امکان ارتباط سریع و آسان با آنها را برای مردم عزیز کشورمان ایجاد کنیم.
                                </Text>
                                <Text style={[styles.Cardtext,{color:'#61d6cd',marginTop:10}]}>
                                    سامانه مشاوره آنلاین تاپمو اولین و حرفه ای ترین کلینیک مشاوره و روانشناسی می باشد که در جهت ارتقای سیستم مشاوره حضوری و غیر حضوری شروع به فعالیت کرده است.
                                </Text>
                                <Text style={[styles.Cardtext,{color:'#3f4392',marginTop:10}]}>
                                    تاپمو با همکاری تیم فنی و پشتیبانی حرفه ای خود، استارتاپی را طرح ریزی کرده است که شما با استفاده از آن می توانید در هر کجا که هستید بدون اتلاف وقت و هزینه ی اضافه و در فضایی امن و با کیفیت، با مشاوران مورد نظر خود مرتبط شوید و به صورت آنلاین، تلفنی و یا حضوری از خدمات آنها استفاده کنید. این سیستم کارآمد امکان برقراری ارتباط به صورت چت نوشتاری یا صوتی و همچنین رزرو وقت حضوری یا غیر حضوری را برای مشاوران مختلف ایجاد کرده است.
                                </Text>
                                <Text style={[styles.Cardtext,{color:'#61d6cd',marginTop:10,fontSize:20,fontWeight:'bold'}]}>اهداف تاپمو</Text>
                                <Text  style={[styles.Cardtext,{color:'#3f4392',marginTop:5,}]}>
                                    امروزه در جامعه ما نیاز به آگاهی و کسب دانش و تجربه در زمینه های مختلف برای اقشار گوناگون جامعه به چشم می خورد. چنانچه برای فعالیت در هر زمینه ای علاوه بر داشتن تخصص در آن زمینه باید به برخی علوم و ابزارهای دیگر نیز تسلط کافی داشته باشید. رسیدن به سطح مورد نیاز آگاهی و تخصص در این زمینه ها و ابزارها نیز نیازمند صرف وقت و هزینه ی زیادی می باشد. تاپمو با هدف ایجاد محیطی امن و کارآمد برای افزایش هرچه بیشتر ارتباط مردم با مشاوران و بهره گیری از تخصص و تجربه ی آنها شروع به فعالیت کرده است. و امید دارد با راه اندازی این سیستم، در جهت افزایش سطح آگاهی و پیشرفت جامعه، گام بردارد.
                                </Text>
                                <Text  style={[styles.Cardtext,{color:'#3f4392',marginTop:10}]}>
                                    سامانه مشاوره آنلاین تاپمو آماده ی ارائه ی خدمات مشاوره آنلاین و همچنین خدمات آنلاین نوبت دهی برای مشاوره حضوری و غیر حضوری در سطح کشور می باشد.
                                </Text>
                                <Text  style={[styles.Cardtext,{color:'#3f4392',marginTop:10}]}>
                                    خدمات تاپمو در آغاز بر ارائه ی مشاوره روانشناسی تمرکز دارد. ما در این حیطه کامل ترین خدمات را با همراهی متخصصان روانشناسی و مشاوره در اختیار شما قرار می دهیم. خدمات مشاوره آنلاین در زمینه های گوناگون شامل: مشاوره ازدواج، مشاوره خانواده، مشاوره جنسی و روابط زناشویی، مشاوره کودک و نوجوان، مشاوره فردی، مشاوره اختلالات روانشناختی از جمله افسردگی، اعتماد به نفس، اعتیاد ها و وسواس های رفتاری، اعتیاد به مواد و ... ارائه می شود.
                                </Text>
                                <Text  style={[styles.Cardtext,{color:'#61d6cd',marginTop:10,fontSize:20,fontWeight:'bold'}]}>امکانات تاپمو</Text>
                                <Text  style={[styles.Cardtext,{color:'#3f4392',marginTop:5}]}>
                                    شما می توانید با استفاده از وب سایت، اپلیکیشن و ربات تلگرامی تاپمو از خدمات مشاوره آنلاین ما استفاده کنید.
                                </Text>
                                <Text  style={[styles.Cardtext,{color:'#3f4392',marginTop:10}]}>
                                    خدمات ارائه شده در تاپمو بسیار گسترده می باشد. شما می توانید در قسمت وبلاگ و مقالات با دانسته های مفید و کاربردی در زمینه ی روانشناسی و مشاوره و راهکار های درمانی و پیشگیری  در این زمینه ها آشنا شوید. با اخبار تاپمو از خبر ها و پیشرفت های روز دنیا در زمینه علوم روانشناسی و مشاوره مطلع خواهید شد. همچنین شما می توانید پس از ثبت نام در سایت، به پنل کاربری شخصی خود راه یابید و از خدمات مشاوره آنلاین تاپمو بهره مند شوید. با استفاده از سیستم رزرو نوبت این امکان را دارید که با شارژ کردن حساب اینترنتی خود و انتخاب مشاور، موضوع، زمان و روش مشاوره، با مشاور مورد نظرتان از طریق یکی از روش های حضوری یا غیر حضوری ارتباط برقرار کنید. همچنین پنل کاربری شما در محیطی امن و خصوصی امکان گفتگو به صورت چت را برای شما ایجاد کرده است. همچنین در این سیستم در صورت تمایل می توانید سوابق مشاوره ی خود را بایگانی کرده و در اختیار داشته باشید.
                                </Text>
                            </Card>
                        </View>
                    </Content>
                    <Footer style={styles.footerStyle}>
                        <FooterTab>
                            <Button vertical onPress={()=>this.props.navigation.navigate('Profile')}>
                                <Image source={require('../img/footer/user.png')}/>
                                <Text style={styles.textStyle}>پروفایل</Text>
                            </Button>
                            <Button vertical onPress={()=>this.props.navigation.navigate('Consultantslist')}>
                                <Image source={require('../img/footer/support.png')}/>
                                <Text style={styles.textStyle}>مشاوران</Text>
                            </Button>
                            <Button vertical onPress={()=>this.props.navigation.navigate('Map')}>
                                <Image source={require('../img/footer/gps.png')}/>
                                <Text style={styles.textStyle}>نقشه</Text>
                            </Button>
                            <Button vertical onPress={()=>this.props.navigation.navigate('Reports')}>
                                <Image source={require('../img/footer/newspaper.png')}/>
                                <Text style={styles.textStyle}>گزارشات</Text>
                            </Button>
                            <Button vertical onPress={()=>this.props.navigation.navigate('Home')}>
                                <Image source={require('../img/footer/home.png')}/>
                                <Text style={styles.textStyle}>خانه</Text>
                            </Button>
                        </FooterTab>
                    </Footer>
                </Container>
            </Drawer>
        );
    }
}

const styles = StyleSheet.create({
    footerStyle:{
        backgroundColor:'#3f4392',
        height:65,
    },
    textStyle:{
        color:'#FFF',
        marginTop:3,
        fontSize:13,
    },
    Input:{
        textAlign:'right',
        borderWidth: 1,
        borderColor: '#999',
        borderRadius:10,
        padding:10,
        // borderTopLeftRadius:20,
    },
    Item:{
        borderBottomColor:'#fff',
    },
    Cardtext:{
        textAlign:'right',
    },
    ModalSelector:{
        margin: 8,
        marginTop:10,
        marginBottom:2,
        backgroundColor:'#61d6cd',
        borderRadius:7,
        borderColor:'transparent'
    }
})

export {AboutusComponent}
