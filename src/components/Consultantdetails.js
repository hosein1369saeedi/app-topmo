
import React, {Component} from 'react';
import {StyleSheet,Image,View} from 'react-native';
import {Container,Header, Left, Button,Text, Card, Grid, Right, Footer, Drawer, Icon, Body, Thumbnail, Col, Content, List, ListItem } from 'native-base';
import {SideBar} from './index';
import Modal from 'react-native-modal';
import { Badge,Rating } from 'react-native-elements';
import ModalSelector from 'react-native-modal-selector';

class ConsultantdetailsComponent extends Component {
    state = {
        isModalVisible: false,
    }
    toggleModal = () => {
        this.setState({ isModalVisible: !this.state.isModalVisible });
    }
    constructor(props){
        super(props);
        this.state={
            selected: "key0",
            textInputValue: ''
        }
    }
    onValueChange(value: string) {
        this.setState({
            selected: value
        });
    }
    closeDrawer=()=>{
        this.drawer._root.close()
    }
    openDrawer=()=>{
        this.drawer._root.open()
    }
    render() {
        const {navigate}=this.props.navigation;
        let index = 0;
        const data0 = [
            { key: index++, label: '۱/۴/۱۳۹۸' },
            { key: index++, label: '۲/۴/۱۳۹۸' },
            { key: index++, label: '۳/۴/۱۳۹۸' },
            { key: index++, label: '۴/۴/۱۳۹۸' },
            { key: index++, label: '۵/۴/۱۳۹۸' },
            { key: index++, label: '۶/۴/۱۳۹۸' },
            { key: index++, label: '۷/۴/۱۳۹۸' },
            { key: index++, label: '۸/۴/۱۳۹۸' },
            { key: index++, label: '۹/۴/۱۳۹۸' },
            { key: index++, label: '۱۰/۴/۱۳۹۸' },
            { key: index++, label: '۱۱/۴/۱۳۹۸' },
            { key: index++, label: '۱۲/۴/۱۳۹۸' },
            { key: index++, label: '۱۳/۴/۱۳۹۸' },
            { key: index++, label: '۱۴/۴/۱۳۹۸' },
            { key: index++, label: '۱۵/۴/۱۳۹۸' },
            { key: index++, label: '۱۶/۴/۱۳۹۸' },
            { key: index++, label: '۱۷/۴/۱۳۹۸' },
            { key: index++, label: '۱۸/۴/۱۳۹۸' },
            { key: index++, label: '۱۹/۴/۱۳۹۸' },
            { key: index++, label: '۲۰/۴/۱۳۹۸' },
            { key: index++, label: '۲۱/۴/۱۳۹۸' },
            { key: index++, label: '۲۲/۴/۱۳۹۸' },
            { key: index++, label: '۲۳/۴/۱۳۹۸' },
            { key: index++, label: '۲۴/۴/۱۳۹۸' },
            { key: index++, label: '۲۵/۴/۱۳۹۸' },
            { key: index++, label: '۲۶/۴/۱۳۹۸' },
            { key: index++, label: '۲۷/۴/۱۳۹۸' },
            { key: index++, label: '۲۸/۴/۱۳۹۸' },
            { key: index++, label: '۲۹/۴/۱۳۹۸' },
            { key: index++, label: '۳۰/۴/۱۳۹۸' },
        ];
        const data1 = [
            { key: index++, label: '۹:۳۰-۱۰:۳۰'  },
            { key: index++, label: '۱۰:۳۰-۱۱:۳۰' },
            { key: index++, label: '۱۱:۳۰-۱۲:۳۰' },
            { key: index++, label: '۱۲:۳۰-۱۳:۳۰' }
        ];
        const data = [
            { key: index++, label: 'تلفنی'},
            { key: index++, label: 'نوشتاری'},
            { key: index++, label: 'حضوری'}
        ];
        const data2 = [
            { key: index++, label: 'تهران'},
            { key: index++, label: 'مشهد'},
            { key: index++, label: 'شیراز'},
            { key: index++, label: 'اصفهان'},
            { key: index++, label: 'تبریز'}
        ];
        const data3 = [
            { key: index++, label: 'مشاور خانواده'},
            { key: index++, label: 'مشاور ازدواج'},
            { key: index++, label: 'مشاور کاری'},
        ];
        return (
            <Drawer
                ref={(ref)=>{this.drawer=ref;}}
                content={<SideBar navigate={navigate}/>}
                onClose={()=>this.closeDrawer()}
            >
                <Container>
                    <Header style={{backgroundColor:'#3f4392'}}>
                        <Left>
                            <Grid>
                                <Col style={{width:'20%'}}>
                                    <Button transparent onPress={()=>this.props.navigation.goBack()}>
                                        <Icon style={{color:'#FFF'}} name="ios-arrow-back" size={30} type="Ionicons"/>
                                    </Button>
                                </Col>
                                <Col style={{width:'50%'}}>
                                    <Button transparent title="Show modal" onPress={this.toggleModal} >
                                        <Icon name="ios-search" type="Ionicons" style={{color:'#FFF',fontSize:30}} />
                                    </Button>
                                    <Modal isVisible={this.state.isModalVisible}>
                                        <View style={{ flex: 1,paddingTop:'30%',}}>
                                            <View style={{backgroundColor:'#FFF',borderRadius:20}}>
                                                <Button 
                                                    style={{
                                                        backgroundColor:'#3f4392',
                                                        borderRadius:20,
                                                        borderBottomLeftRadius:0,
                                                        borderBottomRightRadius:0,
                                                    }}
                                                    title="Hide modal" onPress={this.toggleModal} 
                                                >
                                                    <Icon style={{color:'#FFF',fontSize:25,marginLeft: 10,}} name="close" type="AntDesign"/>
                                                </Button>
                                                <Card style={{paddingBottom:'5%',paddingTop:'5%'}}>
                                                    <Thumbnail source={require('../img/hs.jpg')} style={{marginLeft:'43%',}}/>
                                                    <Text style={{textAlign:'center',fontWeight:'bold',marginTop:15}}>سید حسین سعیدی</Text>
                                                    <Text style={{textAlign:'center',marginTop:5}}>مشاور حقوقی</Text>
                                                </Card>
                                                <ModalSelector
                                                    data={data}
                                                    initValue="انتخاب نوع مشاوره"
                                                    style={[styles.ModalSelector]}
                                                />
                                                <ModalSelector
                                                    data={data2}
                                                    initValue="انتخاب شهر مشاور"
                                                    style={[styles.ModalSelector]}
                                                />
                                                <ModalSelector
                                                    data={data3}
                                                    initValue="انتخاب تخصص مشاور"
                                                    style={[styles.ModalSelector]}
                                                />
                                                <Button full style={{backgroundColor:'#3f4392',fontSize:15,margin:5,borderRadius:20}}>
                                                    <Text>جستجو</Text>
                                                </Button>
                                            </View>  
                                        </View>
                                    </Modal>
                                </Col>
                            </Grid>
                        </Left>
                        <Right>
                            <Grid>
                                <Col style={{width:'75%'}}>
                                    <Button style={{backgroundColor:'transparent',}} onPress={()=>this.props.navigate('Home')} >
                                        <Image source={require('../img/topmo1.png')}/>
                                    </Button>
                                </Col>
                                <Col style={{width:'25%'}}>
                                    <Button transparent onPress={()=>this.openDrawer()}>
                                        <Icon name="navicon" type="FontAwesome" style={{color:'#FFF'}}/>
                                    </Button>
                                </Col>
                            </Grid>
                        </Right>
                    </Header>
                    <Content>
                        {/* <View style={{borderBottomWidth:2,borderBottomColor:'#3f4392',marginTop:30,marginBottom:25}}></View>
                        <View style={{position:'absolute',marginLeft: '3۱%',}}>
                            <Text style={{textAlign:'center',backgroundColor:'#FFF',marginTop:18,color:'#3f4392',fontWeight:'bold',}}>صفحه ی مشاور</Text>
                        </View> */}
                        <View style={[styles.Cardstyle,{}]}>
                            <Left/>
                            <Body>
                                <View style={[,{borderWidth:1,borderColor:'#3f4392',padding:4,borderRadius:50}]}>
                                    <Thumbnail source={require('../img/user.png')} />
                                    <Badge
                                        status="success"
                                        containerStyle={{ position: 'absolute', top:5, right:5, }}
                                    />
                                </View>
                                <Text style={{marginTop:10}}>
                                     سید حسین  
                                     <Text> سعیدی</Text>
                                </Text>
                                <Text note style={{marginTop:5}}>مشاور :<Text note>کسب و کار</Text></Text>
                                <Rating
                                    onFinishRating={this.ratingCompleted}
                                    style={{ paddingVertical: 10, }}
                                    imageSize={15}
                                />
                            </Body>
                            <Right/>
                        </View>
                        <View>
                            <Grid style={{paddingLeft:50,marginTop:-15}}>
                                <Col>
                                    <Button transparent style={[styles.ButtonStyle,{paddingBottom:5,paddingTop:5}]} onPress={()=>this.props.navigation.navigate('Comment')}>
                                        <Text style={[{color:'#000'}]}>۹۹<Text>+</Text></Text>
                                    </Button>
                                    <Text style={[styles.text,{marginLeft:20,marginTop:5}]}>نظرات</Text>
                                </Col>
                                <Col>
                                    <Button transparent style={[styles.ButtonStyle,{}]} onPress={()=>this.props.navigation.navigate('Question')}>
                                        <Text style={[{color:'#000'}]}>۹۹<Text>+</Text></Text>
                                    </Button>
                                    <Text style={[styles.text,{marginTop:5}]}>پرسش و پاسخ</Text>
                                </Col>
                                <Col>
                                    <Button transparent style={[styles.ButtonStyle,{}]}>
                                        <Text style={[{color:'#000'}]}>۹۹<Text>+</Text></Text>
                                    </Button>
                                    <Text style={[styles.text,{marginLeft:20,marginTop:5}]}>مشاوره</Text>
                                </Col>
                            </Grid>
                        </View>
                        <View style={{borderBottomWidth:1,borderBottomColor:'#999',marginTop:13,marginBottom:5}}></View>
                        <View style={{paddingTop:'2%',paddingBottom:'2%',paddingRight:'2%'}}>
                            <Text note style={{textAlign:'right',fontSize:12}}>بیوگرافی</Text>
                            <Text style={{textAlign:'right',fontSize:14}}>
                                طراح وب سایت
                            </Text>
                            <Text style={{textAlign:'right',fontSize:14}}>
                                مشاور در زمینه ی کسب و کار
                            </Text>
                        </View>
                        <View style={{borderBottomWidth:1,borderBottomColor:'#999',marginTop:13,marginBottom:5}}></View>
                        <View>
                            <List style={[styles.ListStyle,{paddingRight:'1.5%'}]}>
                                <ListItem style={{borderBottomWidth:0,marginTop:-10}}>
                                    <Grid>
                                        <Col style={{width:'25%'}}>
                                            <Button full style={{backgroundColor:'#2E8A53',borderRadius:20,borderTopRightRadius:0,borderBottomRightRadius:0,height:39}}>
                                                <Icon name="search1" type="AntDesign"/>
                                            </Button>
                                        </Col>
                                        <Col style={{width:'37.5%'}}>
                                            <ModalSelector
                                                data={data1}
                                                initValue="انتخاب ساعت"
                                                style={[{color:'#fff',borderRadius:0}]}
                                            />
                                        </Col>
                                        <Col style={{width:'37.5%'}}>
                                            <ModalSelector
                                                data={data0}
                                                initValue="انتخاب روز"
                                            />
                                        </Col>
                                    </Grid>
                                </ListItem>
                            </List>
                            <List style={[styles.ListStyle,{}]}>
                                <ListItem style={[styles.ListItem,{}]}>
                                    <Left style={[{width:'10%'}]}>
                                        <Text style={{textAlign:'right',color:'#61d6cd'}}>از ۱۱:۰۰ تا ۱۸:۰۰</Text>
                                    </Left>
                                    <Body style={[{width:'88%'}]}>
                                        <Text style={{textAlign:'right',marginRight:-20,color:'#61d6cd'}}>شنبه</Text>
                                    </Body>
                                    <Right style={[{width:'2%'}]}>
                                        <Badge
                                            status="success"
                                            containerStyle={{ top:0, right: 0}}
                                        />
                                    </Right>
                                </ListItem>
                            </List>
                            <List style={[styles.ListStyle,{}]}>
                                <ListItem style={[styles.ListItem,{backgroundColor:'#eee',}]}>
                                    <Left style={[{width:'10%'}]}>
                                        <Text style={{textAlign:'right',color:'#3f4392'}}>بدون ساعت کاری</Text>
                                    </Left>
                                    <Body style={[{width:'88%'}]}>
                                        <Text style={{textAlign:'right',marginRight:-20,color:'#3f4392'}}>یک شنبه</Text>
                                    </Body>
                                    <Right style={[{width:'2%'}]}>
                                        <Badge
                                            status="error"
                                            containerStyle={{ top:0, right: 0}}
                                        />
                                    </Right>
                                </ListItem>
                            </List>
                            <List style={[styles.ListStyle,{}]}>
                                <ListItem style={[styles.ListItem,{}]}>
                                    <Left style={[{width:'10%'}]}>
                                        <Text style={{textAlign:'right',color:'#61d6cd'}}>از ۱۰:۰۰ تا ۱۲:۳۰</Text>
                                    </Left>
                                    <Body style={[{width:'88%'}]}>
                                        <Text style={{textAlign:'right',marginRight:-20,color:'#61d6cd'}}>دو شنبه</Text>
                                    </Body>
                                    <Right style={[{width:'2%'}]}>
                                        <Badge
                                            status="success"
                                            containerStyle={{ top:0, right: 0}}
                                        />
                                    </Right>
                                </ListItem>
                            </List>
                            <List style={[styles.ListStyle,{}]}>
                                <ListItem style={[styles.ListItem,{}]}>
                                    <Left style={[{width:'10%'}]}>
                                        <Text style={{textAlign:'right',color:'#61d6cd'}}>از ۱۰:۰۰ تا ۱۲:۳۰</Text>
                                    </Left>
                                    <Body style={[{width:'88%'}]}>
                                        <Text style={{textAlign:'right',marginRight:-20,color:'#61d6cd'}}>سه شنبه</Text>
                                    </Body>
                                    <Right style={[{width:'2%'}]}>
                                        <Badge
                                            status="success"
                                            containerStyle={{ top:0, right: 0}}
                                        />
                                    </Right>
                                </ListItem>
                            </List>
                            <List style={[styles.ListStyle,{}]}>
                                <ListItem style={[styles.ListItem,{}]}>
                                    <Left style={[{width:'10%'}]}>
                                        <Text style={{textAlign:'right',color:'#61d6cd'}}>از ۱۰:۰۰ تا ۱۲:۳۰</Text>
                                    </Left>
                                    <Body style={[{width:'88%'}]}>
                                        <Text style={{textAlign:'right',marginRight:-20,color:'#61d6cd'}}>چهار شنبه</Text>
                                    </Body>
                                    <Right style={[{width:'2%'}]}>
                                        <Badge
                                            status="success"
                                            containerStyle={{ top:0, right: 0}}
                                        />
                                    </Right>
                                </ListItem>
                            </List>
                            <List style={[styles.ListStyle,{}]}>
                                <ListItem style={[styles.ListItem,{}]}>
                                    <Left style={[{width:'10%'}]}>
                                        <Text style={{textAlign:'right',color:'#61d6cd'}}>از ۱۰:۰۰ تا ۱۲:۳۰</Text>
                                    </Left>
                                    <Body style={[{width:'88%'}]}>
                                        <Text style={{textAlign:'right',marginRight:-20,color:'#61d6cd'}}>پنج شنبه</Text>
                                    </Body>
                                    <Right style={[{width:'2%'}]}>
                                        <Badge
                                            status="success"
                                            containerStyle={{ top:0, right: 0}}
                                        />
                                    </Right>
                                </ListItem>
                            </List>
                            <List style={[styles.ListStyle,{}]}>
                                <ListItem style={[styles.ListItem,{backgroundColor:'#eee',}]}>
                                    <Left style={[{width:'10%'}]}>
                                        <Text style={{textAlign:'right',color:'#3f4392'}}>از ۱۰:۰۰ تا ۱۲:۳۰</Text>
                                    </Left>
                                    <Body style={[{width:'88%'}]}>
                                        <Text style={{textAlign:'right',marginRight:-20,color:'#3f4392'}}>جمعه</Text>
                                    </Body>
                                    <Right style={[{width:'2%'}]}>
                                        <Badge
                                            status="error"
                                            containerStyle={{ top:0, right: 0}}
                                        />
                                    </Right>
                                </ListItem>
                            </List>
                        </View>
                    </Content>
                    <Footer style={[{backgroundColor:'transparent',borderColor:'transparent',paddingTop:5}]}>
                        <Button style={[styles.BtnStyle,{}]} onPress={()=>this.props.navigation.navigate('ChatRoom')}>
                            <Icon style={[styles.footerIcon,{}]} name="chat" type="Entypo" />
                        </Button>
                        <Button style={[styles.BtnStyle,{}]}>
                            <Icon style={[styles.footerIcon,{}]} name="location" type="Entypo" />
                        </Button>
                        <Button style={[styles.BtnStyle,{}]}>
                            <Text style={[styles.footerText,{}]}>تماس(۱۰۰۰ت/دقیقه)</Text>
                            <Icon style={[styles.footerIcon,{}]} name="phone" type="Entypo" />
                        </Button>
                    </Footer>
                </Container>
            </Drawer>
        );
    }
}

const styles = StyleSheet.create({
    footerStyle:{
        backgroundColor:'#3f4392',
        height:65,
    },
    textStyle:{
        color:'#FFF',
        marginTop:3,
        fontSize:13,
    },
    Cardstyle:{
        paddingVertical:20,
    },
    ButtonStyle:{
        borderRadius:50,
        borderWidth:1,
        borderColor:'#3f4392',
        width:65,
        height:65,
    },
    text:{
        fontSize:10,
    },
    BtnStyle:{
        backgroundColor:'#61d6cd',
        marginHorizontal:5,
        borderBottomLeftRadius:20,
        borderBottomRightRadius:20,
        borderTopLeftRadius:20,
        borderTopRightRadius:0,
        shadowColor: '#3f4392',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.8,
        shadowRadius: 2,
    },
    footerIcon:{
        color:'#3f4392'
    },
    footerText:{
        color:'#3f4392'
    },
    ListItem:{
        backgroundColor:'#3f4392',
        borderRadius:10,
        paddingLeft:10,
        borderBottomWidth: 0,
        shadowColor: '#3f4392',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.8,
        shadowRadius: 2,
        elevation: 1,
    }, 
    container: {
        flex: 1,
    },
    contentContainer: {
        height: 1000,
        width: 1000,
    },  
    ListStyle:{
        paddingRight:'5%',
        paddingLeft:'0%',
        marginTop:10
    } 
})

export {ConsultantdetailsComponent}
