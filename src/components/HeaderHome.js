
import React, {Component} from 'react';
import {Image,View,StyleSheet} from 'react-native';
import {Card, Header, Left, Button, Text, Body, Thumbnail, Right, Icon, Drawer} from 'native-base';
import {SideBar} from './index';
import ModalSelector from 'react-native-modal-selector'
import Modal from 'react-native-modal';

class HeaderHomeComponent extends Component {
  state = {
    isModalVisible: false,
  }
  toggleModal = () => {
    this.setState({ isModalVisible: !this.state.isModalVisible });
  }
  constructor(props){
    super(props);
    this.state={
      selected: "key0",
    }
  }
  onValueChange(value: string) {
    this.setState({
      selected: value
    });
  }
  closeDrawer=()=>{
    this.drawer._root.close()
  }
  openDrawer=()=>{
    this.drawer._root.open()
  }
  render() {
    let index = 0;
    const data = [
      { key: index++, label: 'تلفنی'},
      { key: index++, label: 'نوشتاری'},
      { key: index++, label: 'حضوری'}
    ];
    const data2 = [
      { key: index++, label: 'تهران'},
      { key: index++, label: 'مشهد'},
      { key: index++, label: 'شیراز'},
      { key: index++, label: 'اصفهان'},
      { key: index++, label: 'تبریز'}
    ];
    const data3 = [
      { key: index++, label: 'مشاور خانواده'},
      { key: index++, label: 'مشاور ازدواج'},
      { key: index++, label: 'مشاور کاری'},
    ];
    const {navigate}=this.props.navigation;
    return (
      <Drawer
        ref={(ref)=>{this.drawer=ref;}}
        content={<SideBar navigate={navigate}/>}
        onClose={()=>this.closeDrawer()}
      >
        <Header searchBar rounded style={{backgroundColor:'#3f4392',height:200,position:'relative',}}>
          <Left style={{marginTop:-140,}}>
            <Button transparent title="Show modal" onPress={this.toggleModal} >
              <Icon name="ios-search" type="Ionicons" style={{color:'#FFF',fontSize:30}} />
            </Button>
            <Modal isVisible={this.state.isModalVisible}>
              <View style={{ flex: 1,paddingTop:'30%',}}>
                <View style={{backgroundColor:'#FFF',borderRadius:20}}>
                  <Button
                    style={{
                      backgroundColor:'#3f4392',
                      borderRadius:20,
                      borderBottomLeftRadius:0,
                      borderBottomRightRadius:0,
                    }}
                    title="Hide modal" onPress={this.toggleModal} 
                  >
                    <Icon style={{color:'#FFF',fontSize:25,marginLeft: 10,}} name="close" type="AntDesign"/>
                  </Button>
                  <Card style={{paddingBottom:'5%',paddingTop:'5%'}}>
                    <Thumbnail source={require('../img/hs.jpg')} style={{marginLeft:'43%',}}/>
                    <Text style={{textAlign:'center',fontWeight:'bold',marginTop:15}}>سید حسین سعیدی</Text>
                    <Text style={{textAlign:'center',marginTop:5}}>مشاور حقوقی</Text>
                  </Card>
                  <ModalSelector
                    data={data}
                    initValue="انتخاب نوع مشاوره"
                    style={[styles.ModalSelector]}
                  />
                  <ModalSelector
                    data={data2}
                    initValue="انتخاب شهر مشاور"
                    style={[styles.ModalSelector]}
                  />
                  <ModalSelector
                    data={data3}
                    initValue="انتخاب تخصص مشاور"
                    style={[styles.ModalSelector]}
                  />
                  <Button full style={{backgroundColor:'#3f4392',fontSize:15,margin:5,borderBottomLeftRadius:20,borderBottomEndRadius:20}}>
                    <Text>جستجو</Text>
                  </Button>
                </View>  
              </View>
            </Modal>
          </Left>
          <Body>
            <View style={[styles.shadowStyle,{}]}>
              <Image source={require('../img/topmo.png')}/>
            </View>
          </Body>
          <Badge value="۱" status="error" containerStyle={{ position: 'absolute', top: 45, right: 30,zIndex:1}}/>
          <Right style={{marginTop:-140,}}>
            <Button transparent onPress={()=>this.openDrawer()}>
              <Icon name="navicon" type="FontAwesome" style={{color:'#FFF'}}/>
            </Button>
          </Right>
        </Header>
      </Drawer>
    );
  }
}
const styles = StyleSheet.create({
  contentStyle:{
    position:'relative',
    zIndex:-1,
  },
  buttonStyle:{
    width:"100%",
    height:50,
    padding:0,
    marginTop:6,
    backgroundColor:'#FFF',
    textAlign:'center',
    paddingLeft:'20%',
  },
  buyStyle:{
    color:'#3f4392',
    fontSize:25,
    textAlign:'center',
  },
  shadowStyle:{
    backgroundColor:'rgba(255, 255, 255, 0.7)',
    padding:'40% 0%',
    borderRadius:25,
    marginTop:'130%',
    height:200,
    borderColor: '#3f4392',
    borderBottomWidth: 0,
    shadowColor: '#3f4392',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.5,
    shadowRadius: 10,
    elevation: 1,
    marginLeft: 9,
    marginRight: 5,
  },
  ModalSelector:{
    margin:5,
    padding:5,
  },
})
export {HeaderHomeComponent}
