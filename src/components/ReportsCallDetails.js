
import React, {Component} from 'react';
import {StyleSheet,Image,View} from 'react-native';
import {Container,Header, Left, Button,Text, Card, Grid, Right, Footer, Drawer, Icon, Body, Thumbnail, Col, Content, List, ListItem } from 'native-base';
import {SideBar} from './index';
import { Badge,Rating, AirbnbRating } from 'react-native-elements'
import ModalSelector from 'react-native-modal-selector';
import Modal from 'react-native-modal';

class ReportsCallDetailsComponent extends Component {
    state = {
        isModalVisible: false,
    };
    toggleModal = () => {
        this.setState({ isModalVisible: !this.state.isModalVisible });
    };
    constructor(props){
        super(props);
        this.state={
            selected: "key0",
            textInputValue: ''
        }
    }
    onValueChange(value: string) {
        this.setState({
            selected: value
        });
    }
    closeDrawer=()=>{
        this.drawer._root.close()
    }
    openDrawer=()=>{
        this.drawer._root.open()
    }
    render() {
        let index = 0;
        const data = [
            { key: index++, label: 'تلفنی'},
            { key: index++, label: 'نوشتاری'},
            { key: index++, label: 'حضوری'}
        ];
        const data2 = [
            { key: index++, label: 'تهران'},
            { key: index++, label: 'مشهد'},
            { key: index++, label: 'شیراز'},
            { key: index++, label: 'اصفهان'},
            { key: index++, label: 'تبریز'}
        ];
        const data3 = [
            { key: index++, label: 'مشاور خانواده'},
            { key: index++, label: 'مشاور ازدواج'},
            { key: index++, label: 'مشاور کاری'},
        ];
        const {navigate}=this.props.navigation;
        return (
            <Drawer
            ref={(ref)=>{this.drawer=ref;}}
            content={<SideBar navigate={navigate}/>}
            onClose={()=>this.closeDrawer()}
            >
                <Container>
                    <Header style={{backgroundColor:'#3f4392'}}>
                        <Left>
                            <Grid>
                                <Col style={{width:'20%'}}>
                                    <Button transparent>
                                        <Icon style={{color:'#FFF'}} name="ios-arrow-back" size={30} type="Ionicons"/>
                                    </Button>
                                </Col>
                                <Col style={{width:'50%'}}>
                                    <Button transparent title="Show modal" onPress={this.toggleModal} >
                                        <Icon name="search" type="FontAwesome" style={{color:'#FFF',fontSize:30}} />
                                    </Button>
                                    <Modal isVisible={this.state.isModalVisible}>
                                        <View style={{ flex: 1,paddingTop:'30%',}}>
                                            <View style={{backgroundColor:'#FFF',borderRadius:20}}>
                                                <Button
                                                    style={{
                                                        backgroundColor:'#3f4392',
                                                        borderRadius:20,
                                                        borderBottomLeftRadius:0,
                                                        borderBottomRightRadius:0,
                                                        width:'100%'
                                                    }}
                                                    title="Hide modal" onPress={this.toggleModal} 
                                                >
                                                    <Icon style={{color:'#FFF',fontSize:25,marginLeft: 10,}} name="close" type="AntDesign"/>
                                                </Button>
                                                <Card style={{paddingBottom:'5%',paddingTop:'5%'}}>
                                                    <Thumbnail source={require('../img/hs.jpg')} style={{marginLeft:'43%',}}/>
                                                    <Text style={{textAlign:'center',fontWeight:'bold',marginTop:15}}>سید حسین سعیدی</Text>
                                                    <Text style={{textAlign:'center',marginTop:5}}>مشاور حقوقی</Text>
                                                </Card>
                                                <ModalSelector
                                                    data={data}
                                                    initValue="انتخاب نوع مشاوره"
                                                    stylقe={[styles.ModalSelector]}
                                                />
                                                <ModalSelector
                                                    data={data2}
                                                    initValue="انتخاب شهر مشاور"
                                                    style={[styles.ModalSelector]}
                                                />
                                                <ModalSelector
                                                    data={data3}
                                                    initValue="انتخاب تخصص مشاور"
                                                    style={[styles.ModalSelector]}
                                                />
                                                <Button full style={{backgroundColor:'#3f4392',fontSize:15,margin:5,borderBottomLeftRadius:20,borderBottomEndRadius:20}}>
                                                    <Text>جستجو</Text>
                                                </Button>
                                            </View>  
                                        </View>
                                    </Modal>
                                </Col>
                            </Grid>
                        </Left>
                        <Right>
                            <Grid>
                                <Col style={{width:'75%'}}>
                                    <Button style={{backgroundColor:'transparent',}} onPress={()=>this.props.navigate('Home')} >
                                        <Image source={require('../img/topmo1.png')}/>
                                    </Button>
                                </Col>
                                <Col style={{width:'25%'}}>
                                    <Button transparent onPress={()=>this.openDrawer()}>
                                        <Icon name="navicon" type="FontAwesome" style={{color:'#FFF'}}/>
                                    </Button>
                                </Col>
                            </Grid>
                        </Right>
                    </Header>
                    <Content>
                        <View style={[styles.Cardstyle,{}]}>
                            <Left/>
                            <Body>
                                <View style={[,{borderWidth:1,borderColor:'#3f4392',padding:4,borderRadius:50}]}>
                                    <Thumbnail source={require('../img/user.png')} />
                                </View>
                                <Text style={{marginTop:10}}>
                                     سید حسین  
                                     <Text> سعیدی</Text>
                                </Text>
                                <Text note style={{marginTop:5}}>مشاور :<Text note>کسب و کار</Text></Text>
                            </Body>
                            <Right/>
                        </View>
                        <View>
                            <Button full style={[styles.ButtonStyle,{}]} onPress={()=>this.props.navigation.navigate('Comment')}>
                                <Text style={[{color:'#3f4392'}]}>مشاهده سابقه ی چت</Text>
                                <Icon style={[styles.footerIcon,{}]}  name="chat" type="Entypo" />
                            </Button>
                        </View>
                        <View style={{borderBottomWidth:1,borderBottomColor:'#999',marginTop:13,marginBottom:5}}></View>
                        <View>
                            <List style={[{paddingRight:'5%',paddingLeft:'0%',marginTop:10}]}>
                                <ListItem style={[styles.ListItem,{}]}>
                                    <Left style={[{width:'33%'}]}>
                                        <Text style={{textAlign:'right',color:'#61d6cd'}}>۱۷:۲۵</Text>
                                    </Left>
                                    <Body style={[{width:'33%'}]}>
                                        <Text style={{textAlign:'right',color:'#61d6cd'}}>۱۰/۱۰/۹۷</Text>
                                    </Body>
                                    <Right style={[{width:'33%'}]}>
                                        <Text style={{textAlign:'right',color:'#61d6cd'}}>شنبه</Text>
                                    </Right>
                                </ListItem>
                            </List>
                            <List style={[{paddingRight:'5%',paddingLeft:'0%',marginTop:10}]}>
                                <ListItem style={[styles.ListItem,{}]}>
                                    <Left style={[{width:'33%'}]}>
                                        <Text style={{textAlign:'right',color:'#61d6cd'}}>۱۷:۲۵</Text>
                                    </Left>
                                    <Body style={[{width:'33%'}]}>
                                        <Text style={{textAlign:'right',color:'#61d6cd'}}>۱۰/۱۰/۹۷</Text>
                                    </Body>
                                    <Right style={[{width:'33%'}]}>
                                        <Text style={{textAlign:'right',color:'#61d6cd'}}>شنبه</Text>
                                    </Right>
                                </ListItem>
                            </List>
                        </View>
                    </Content>
                    <Footer style={[{backgroundColor:'transparent',borderColor:'transparent',paddingTop:5}]}>
                        <Button style={[styles.BtnStyle,{borderTopLeftRadius:0,}]} onPress={()=>this.props.navigation.navigate('ChatRoom')}>
                            <Icon style={[styles.footerIcon,{}]}  name="chat" type="Entypo" />
                        </Button>
                        <Button style={[styles.BtnStyle,{}]}>
                            <Icon style={[styles.footerIcon,{}]}  name="location" type="Entypo" />
                        </Button>
                        <Button style={[styles.BtnStyle,{borderTopRightRadius:0,}]}>
                            <Text style={[styles.footerText,{}]}>تماس(۱۰۰۰ت/دقیقه)</Text>
                            <Icon style={[styles.footerIcon,{}]}  name="phone" type="Entypo" />
                        </Button>
                    </Footer>
                </Container>
            </Drawer>
        );
    }
}

const styles = StyleSheet.create({
    footerStyle:{
        backgroundColor:'#3f4392',
        height:65,
    },
    textStyle:{
        color:'#FFF',
        marginTop:3,
        fontSize:13,
    },
    Cardstyle:{
        paddingVertical:20,
    },
    ButtonStyle:{
        borderRadius:10,
        borderWidth:1,
        borderColor:'#61d6cd',
        backgroundColor:'#61d6cd',
        paddingBottom:5,
        paddingTop:5,
        marginLeft:15,
        marginRight:15,
    },
    text:{
        fontSize:10,
    },
    BtnStyle:{
        backgroundColor:'#61d6cd',
        marginHorizontal:5,
        borderRadius:20,
        shadowColor: '#3f4392',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.8,
        shadowRadius: 2,
    },
    footerIcon:{
        color:'#3f4392'
    },
    footerText:{
        color:'#3f4392'
    },
    ListItem:{
        backgroundColor:'#3f4392',
        borderRadius:10,
        paddingLeft:10,
        borderBottomWidth: 0,
        shadowColor: '#3f4392',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.8,
        shadowRadius: 2,
        elevation: 1,
    },
    
})

export {ReportsCallDetailsComponent}
