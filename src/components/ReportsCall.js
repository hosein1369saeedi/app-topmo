
import React, {Component} from 'react';
import Modal from "react-native-modal";
import {View,StyleSheet,Image} from 'react-native';
import {Container,Header, Left, Button,Text, Body, Grid, Right, Content, Footer, FooterTab, Drawer, Icon, Col, List, ListItem,} from 'native-base';
import {SideBar} from './index';
import  {Avatar,Badge}  from 'react-native-elements';


class ReportsCallComponent extends Component {
    closeDrawer=()=>{
        this.drawer._root.close()
    }
    openDrawer=()=>{
        this.drawer._root.open()
    }
    render() {
        const {navigate}=this.props.navigation;
        return (
            <Drawer
            ref={(ref)=>{this.drawer=ref;}}
            content={<SideBar navigate={navigate}/>}
            onClose={()=>this.closeDrawer()}
            >
                <Container>
                    <Header style={{backgroundColor:'#3f4392'}}>
                        <Left>
                            <Grid>
                                <Col style={{width:'20%'}}>
                                    <Button transparent>
                                        <Icon style={{color:'#FFF'}} name="ios-arrow-back" size={30} type="Ionicons"/>
                                    </Button>
                                </Col>
                                <Col style={{width:'50%'}}>
                                    <Button transparent>
                                        <Icon name="ios-search" style={{color:'#FFF'}} />
                                    </Button>
                                </Col>
                            </Grid>
                        </Left>
                        <Right>
                            <Grid>
                                <Col style={{width:'75%'}}>
                                    <Button style={{backgroundColor:'transparent',}} onPress={()=>this.props.navigate('Home')} >
                                        <Image source={require('../img/topmo1.png')}/>
                                    </Button>
                                </Col>
                                <Col style={{width:'25%'}}>
                                    <Button transparent onPress={()=>this.openDrawer()}>
                                        <Icon name="navicon" type="FontAwesome" style={{color:'#FFF'}}/>
                                    </Button>
                                </Col>
                            </Grid>
                        </Right>
                    </Header>
                    <Content>
                    <View style={{borderBottomWidth:2,borderBottomColor:'#3f4392',marginTop:30,marginBottom:25}}></View>
                        <View style={{position:'absolute',marginLeft: '30%',}}>
                            <Text style={{textAlign:'center',backgroundColor:'#FFF',marginTop:18,color:'#3f4392',fontWeight:'bold',}}>گزارش تماس ها</Text>
                        </View>
                        <View style={[,{}]}>
                            <List style={[styles.listStyle,{}]}>
                                <ListItem style={[styles.listItemStyle,{}]} onPress={()=>this.props.navigation.navigate('ReportsCallDetails')}>
                                    <Left>
                                        <Text style={[styles.ListItemText,{}]}><Text style={[styles.ListItemText,{}]}>۱۰/۱۰/۹۷</Text> - <Text style={[styles.ListItemText,{}]}>۱۲:۳۰</Text></Text>
                                    </Left>
                                    <Body>
                                        <Text style={[styles.text,{fontWeight:'bold'}]}>سید حسین سعیدی</Text>
                                        <Text style={[styles.text,{}]} note>مشاور خانواده</Text>
                                    </Body>
                                    <Right style={[styles.rightStyle,{}]}>
                                        <Avatar
                                            rounded
                                            source={require('../img/user.png')}
                                        />
                                    </Right>
                                </ListItem>
                                <ListItem style={[styles.listItemStyle,{}]} onPress={()=>this.props.navigation.navigate('ReportsCallDetails')}>
                                    <Left>
                                        <Text style={[styles.ListItemText,{}]}><Text style={[styles.ListItemText,{}]}>۱۰/۱۰/۹۷</Text> - <Text style={[styles.ListItemText,{}]}>۱۲:۳۰</Text></Text>
                                    </Left>
                                    <Body>
                                        <Text style={[styles.text,{fontWeight:'bold'}]}>سید حسین سعیدی</Text>
                                        <Text style={[styles.text,{}]} note>مشاور خانواده</Text>
                                    </Body>
                                    <Right style={[styles.rightStyle,{}]}>
                                        <Avatar
                                            rounded
                                            source={require('../img/user.png')}
                                        />
                                    </Right>
                                </ListItem>
                                <ListItem style={[styles.listItemStyle,{}]} onPress={()=>this.props.navigation.navigate('ReportsCallDetails')}>
                                    <Left>
                                        <Text style={[styles.ListItemText,{}]}><Text style={[styles.ListItemText,{}]}>۱۰/۱۰/۹۷</Text> - <Text style={[styles.ListItemText,{}]}>۱۲:۳۰</Text></Text>
                                    </Left>
                                    <Body>
                                        <Text style={[styles.text,{fontWeight:'bold'}]}>سید حسین سعیدی</Text>
                                        <Text style={[styles.text,{}]} note>مشاور خانواده</Text>
                                    </Body>
                                    <Right style={[styles.rightStyle,{}]}>
                                        <Avatar
                                            rounded
                                            source={require('../img/user.png')}
                                        />
                                    </Right>
                                </ListItem>
                                <ListItem style={[styles.listItemStyle,{}]} onPress={()=>this.props.navigation.navigate('ReportsCallDetails')}>
                                    <Left>
                                        <Text style={[styles.ListItemText,{}]}><Text style={[styles.ListItemText,{}]}>۱۰/۱۰/۹۷</Text> - <Text style={[styles.ListItemText,{}]}>۱۲:۳۰</Text></Text>
                                    </Left>
                                    <Body>
                                        <Text style={[styles.text,{fontWeight:'bold'}]}>سید حسین سعیدی</Text>
                                        <Text style={[styles.text,{}]} note>مشاور خانواده</Text>
                                    </Body>
                                    <Right style={[styles.rightStyle,{}]}>
                                        <Avatar
                                            rounded
                                            source={require('../img/user.png')}
                                        />
                                    </Right>
                                </ListItem>
                            </List>
                        </View>
                    </Content>
                    <Footer style={styles.footerStyle}>
                        <FooterTab>
                            <Button vertical onPress={()=>this.props.navigation.navigate('Form')}>
                                <Image source={require('../img/footer/user.png')}/>
                                <Text style={styles.textStyle}>پروفایل</Text>
                            </Button>
                            <Button vertical>
                                <Image source={require('../img/footer/support.png')}/>
                                <Text style={styles.textStyle}>مشاوران</Text>
                            </Button>
                            <Button vertical>
                                <Image source={require('../img/footer/gps.png')}/>
                                <Text style={styles.textStyle}>نقشه</Text>
                            </Button>
                            <Button vertical>
                                <Image source={require('../img/footer/newspaper.png')}/>
                                <Text style={styles.textStyle}>گزارشات</Text>
                            </Button>
                            <Button vertical>
                                <Image source={require('../img/footer/home.png')}/>
                                <Text style={styles.textStyle}>خانه</Text>
                            </Button>
                        </FooterTab>
                    </Footer>
                </Container>
            </Drawer>
        );
    }
}

const styles = StyleSheet.create({
    footerStyle:{
        backgroundColor:'#3f4392',
        height:65,
    },
    textStyle:{
        color:'#FFF',
        marginTop:3,
        fontSize:13
    },
    listStyle:{
        paddingVertical:15
    },
    listItemStyle:{
        backgroundColor:'#3f4392',
        marginRight:'3%',
        marginLeft:'3%',
        borderRadius:20,
        paddingLeft:10,
        marginTop:5,
    },
    text:{
        textAlign:'right',
        color:'#61d6cd',
        fontSize:13,
        marginRight:22,
        marginTop:0
    },
    rightStyle:{
        backgroundColor:'transparent',
        paddingRight:6,
        paddingTop:5,
        paddingBottom:5,
        borderRadius:50,
        borderWidth: 1,
        borderColor:'#61d6cd',
    },
    ListItemText:{
        marginTop:15,
        color:'#61d6cd',
        fontSize:12,
        marginLeft:10
    },
})

export {ReportsCallComponent}
