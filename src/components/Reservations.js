
import React, {Component} from 'react';
import {View,Image,TouchableOpacity,StyleSheet} from 'react-native';
import {Icon as Icn} from 'react-native-vector-icons/FontAwesome';
import {Container,Header, Left, Button,Text, Grid, Col, Right, Content, Footer, FooterTab, Drawer, Icon, Card, Input, Spinner, Form, Picker, List, ListItem, data, Thumbnail, Tabs, Tab, TabHeading, ScrollableTab, Body} from 'native-base';
import {SideBar} from './index';
import Modal from 'react-native-modal';
import {Avatar} from 'react-native-elements';

class ReservationsComponent extends Component {
    state = {
        isModalVisible: false,
    };

    _toggleModal = () =>
    this.setState({ isModalVisible: !this.state.isModalVisible });
    
    constructor(props){
        super(props);
        this.state={
            selected: "key0",
        }
    }
    onValueChange(value: string) {
        this.setState({
            selected: value
        });
    }
    closeDrawer=()=>{
        this.drawer._root.close()
    }
    openDrawer=()=>{
        this.drawer._root.open()
    }
    render() {
        const {navigate}=this.props.navigation;
        return (
            <Drawer
            ref={(ref)=>{this.drawer=ref;}}
            content={<SideBar navigate={navigate}/>}
            onClose={()=>this.closeDrawer()}
            >
                <Container>
                    <Header style={{backgroundColor:'#3f4392'}}>
                        <Left>
                            <Grid>
                                <Col style={{width:'20%'}}>
                                    <Button transparent>
                                        <Icon style={{color:'#FFF'}} name="ios-arrow-back" size={30} type="Ionicons"/>
                                    </Button>
                                </Col>
                                <Col style={{width:'50%'}}>
                                    <TouchableOpacity style={{marginTop:10,marginLeft:10}} onPress={this._toggleModal}>
                                        <Icon name="ios-search" style={{color:'#FFF'}} />
                                    </TouchableOpacity>
                                    <Modal isVisible={this.state.isModalVisible} style={{backgroundColor:'#FFF',borderRadius:20,flex:1}}>
                                        <View style={{ flex: 1 }}>
                                            <Header style={{paddingBottom:10,backgroundColor:'#3f4392',borderTopLeftRadius:20,borderTopRightRadius:20}}>
                                                <Left>
                                                    <TouchableOpacity onPress={this._toggleModal}>
                                                        <Icon style={{color:'#FFF',fontSize:25,marginLeft: 10,}} name="close" type="AntDesign"/>
                                                    </TouchableOpacity>
                                                </Left>
                                                <Right></Right>
                                            </Header>
                                            <View>
                                                <Card style={{paddingBottom:'5%',paddingTop:'5%'}}>
                                                    <Thumbnail source={require('../img/hs.jpg')} style={{marginLeft:'43%',}}/>
                                                    <Text style={{textAlign:'center',fontWeight:'bold',marginTop:15}}>سید حسین سعیدی</Text>
                                                    <Text style={{textAlign:'center',marginTop:5}}>مشاور حقوقی</Text>
                                                </Card>
                                                <Text style={{textAlign:'right',color:'#3f4392',paddingRight: 15,}}>انتخاب نوع مشاوره :</Text>
                                                <Picker
                                                mode="dropdown"
                                                iosHeader="نوع مشاوره"
                                                iosIcon={<Icon name="arrow-down" />}
                                                style={{ width:'100%',borderBottomWidth:1,borderBottomColor:'#999',textAlign:'right'}}
                                                selectedValue={this.state.selected}
                                                onValueChange={this.onValueChange.bind(this)}
                                                >
                                                    <Picker.Item label="انتخاب" value="key0" />
                                                    <Picker.Item label="حضوری" value="key1" />
                                                    <Picker.Item label="تلفنی" value="key2" />
                                                    <Picker.Item label="نوشتاری" value="key3" />
                                                </Picker>
                                                <Text style={{textAlign:'right',color:'#3f4392',paddingRight: 15,}}>انتخاب شهر :</Text>
                                                <Picker
                                                mode="dropdown"
                                                iosHeader="نوع مشاوره"
                                                iosIcon={<Icon name="arrow-down" />}
                                                style={{ width:'100%',borderBottomWidth:1,borderBottomColor:'#999',textAlign:'right'}}
                                                selectedValue={this.state.selected}
                                                onValueChange={this.onValueChange.bind(this)}
                                                >
                                                    <Picker.Item label="انتخاب" value="key0" />
                                                    <Picker.Item label="حضوری" value="key1" />
                                                    <Picker.Item label="تلفنی" value="key2" />
                                                    <Picker.Item label="نوشتاری" value="key3" />
                                                </Picker>
                                                <Text style={{textAlign:'right',color:'#3f4392',paddingRight: 15,}}>انتخاب تخصص مشاور :</Text>
                                                <Picker
                                                mode="dropdown"
                                                iosHeader="نوع مشاوره"
                                                iosIcon={<Icon name="arrow-down" />}
                                                style={{ width:'100%',borderBottomWidth:1,borderBottomColor:'#999',textAlign:'right'}}
                                                selectedValue={this.state.selected}
                                                onValueChange={this.onValueChange.bind(this)}
                                                >
                                                    <Picker.Item label="انتخاب" value="key0" />
                                                    <Picker.Item label="حضوری" value="key1" />
                                                    <Picker.Item label="تلفنی" value="key2" />
                                                    <Picker.Item label="نوشتاری" value="key3" />
                                                </Picker>
                                                <Button full style={{backgroundColor:'#3f4392',fontSize:15,margin:5,borderRadius:10}}>
                                                    <Text>جستجو</Text>
                                                </Button>
                                            </View>                            
                                        </View>
                                    </Modal>
                                </Col>
                            </Grid>
                        </Left>
                        <Right>
                            <Grid>
                                <Col style={{width:'75%'}}>
                                    <Button style={{backgroundColor:'transparent',}} onPress={()=>this.props.navigate('Home')} >
                                        <Image source={require('../img/topmo1.png')}/>
                                    </Button>
                                </Col>
                                <Col style={{width:'25%'}}>
                                    <Button transparent onPress={()=>this.openDrawer()}>
                                        <Icon name="navicon" type="FontAwesome" style={{color:'#FFF'}}/>
                                    </Button>
                                </Col>
                            </Grid>
                        </Right>
                    </Header>
                    <Content>
                        <View style={{borderBottomWidth:2,borderBottomColor:'#3f4392',marginTop:30,marginBottom:25}}></View>
                        <View style={{position:'absolute',marginLeft: '33%',}}>
                            <Text style={{textAlign:'center',backgroundColor:'#FFF',marginTop:18,color:'#3f4392',fontWeight:'bold',}}>رزرو ها</Text>
                        </View>
                        <Tabs initialPage={1}>
                            <Tab style={[styles.tab,{}]} heading={
                                <TabHeading>
                                    <Text style={{fontSize:17}}>رزرو نوشتاری</Text>
                                    <Icon name="pencil" type="FontAwesome" style={{fontSize:17}}/>
                                </TabHeading>
                            }>
                                <View style={[styles.cardStyle,{}]}>
                                    <List style={[styles.listStyle,{}]}>
                                        <ListItem style={[styles.listItemStyle,{}]} onPress={()=>this.props.navigation.navigate('Setting')}>
                                            <Left>
                                                <Text style={[styles.text,{}]} note>۱۰/۱۰/۹۸</Text>
                                                <Text style={[styles.text,{}]}> -</Text>
                                                <Text style={[styles.text,{}]} note> ۱۳:۳۰</Text>
                                            </Left>
                                            <Body>
                                                <Text style={[styles.text,{fontWeight:'bold'}]}>سید حسین سعیدی</Text>
                                                <Text style={[styles.text,{}]} note>مشاور خانواده</Text>
                                            </Body>
                                            <Right style={[styles.rightStyle,{}]}>
                                                <Avatar
                                                    rounded
                                                    source={require('../img/hs.jpg')}
                                                />
                                            </Right>
                                        </ListItem>
                                        <ListItem style={[styles.listItemStyle,{}]}>
                                            <Left>
                                                <Text style={[styles.text,{}]} note>۱۰/۱۰/۹۸</Text>
                                                <Text style={[styles.text,{}]}> -</Text>
                                                <Text style={[styles.text,{}]} note> ۱۳:۳۰</Text>
                                            </Left>
                                            <Body>
                                                <Text style={[styles.text,{fontWeight:'bold'}]}>سید حسین سعیدی</Text>
                                                <Text style={[styles.text,{}]} note>مشاور خانواده</Text>
                                            </Body>
                                            <Right style={[styles.rightStyle,{}]}>
                                                <Avatar
                                                    rounded
                                                    source={require('../img/hs.jpg')}
                                                />
                                            </Right>
                                        </ListItem>
                                        <ListItem style={[styles.listItemStyle,{}]}>
                                            <Left>
                                                <Text style={[styles.text,{}]} note>۱۰/۱۰/۹۸</Text>
                                                <Text style={[styles.text,{}]}> -</Text>
                                                <Text style={[styles.text,{}]} note> ۱۳:۳۰</Text>
                                            </Left>
                                            <Body>
                                                <Text style={[styles.text,{fontWeight:'bold'}]}>سید حسین سعیدی</Text>
                                                <Text style={[styles.text,{}]} note>مشاور خانواده</Text>
                                            </Body>
                                            <Right style={[styles.rightStyle,{}]}>
                                                <Avatar
                                                    rounded
                                                    source={require('../img/hs.jpg')}
                                                />
                                            </Right>
                                        </ListItem>
                                    </List>
                                </View>
                            </Tab>
                            <Tab style={[styles.tab,{}]} heading={
                                <TabHeading>
                                    <Text style={{fontSize:17}}>رزرو تلفنی</Text>
                                    <Icon name="screen-smartphone" type="SimpleLineIcons" style={{fontSize:17}}/>
                                </TabHeading>
                            }>
                                <View style={[styles.cardStyle,{}]}>
                                    <List style={[styles.listStyle,{}]}>
                                        <ListItem style={[styles.listItemStyle,{}]}>
                                            <Left>
                                                <Text style={[styles.text,{}]} note>۱۰/۱۰/۹۸</Text>
                                                <Text style={[styles.text,{}]}> -</Text>
                                                <Text style={[styles.text,{}]} note> ۱۳:۳۰</Text>
                                            </Left>
                                            <Body>
                                                <Text style={[styles.text,{fontWeight:'bold'}]}>سید حسین سعیدی</Text>
                                                <Text style={[styles.text,{}]} note>مشاور خانواده</Text>
                                            </Body>
                                            <Right style={[styles.rightStyle,{}]}>
                                                <Avatar
                                                    rounded
                                                    source={require('../img/hs.jpg')}
                                                />
                                            </Right>
                                        </ListItem>
                                        <ListItem style={[styles.listItemStyle,{}]}>
                                            <Left>
                                                <Text style={[styles.text,{}]} note>۱۰/۱۰/۹۸</Text>
                                                <Text style={[styles.text,{}]}> -</Text>
                                                <Text style={[styles.text,{}]} note> ۱۳:۳۰</Text>
                                            </Left>
                                            <Body>
                                                <Text style={[styles.text,{fontWeight:'bold'}]}>سید حسین سعیدی</Text>
                                                <Text style={[styles.text,{}]} note>مشاور خانواده</Text>
                                            </Body>
                                            <Right style={[styles.rightStyle,{}]}>
                                                <Avatar
                                                    rounded
                                                    source={require('../img/hs.jpg')}
                                                />
                                            </Right>
                                        </ListItem>
                                        <ListItem style={[styles.listItemStyle,{}]}>
                                            <Left>
                                                <Text style={[styles.text,{}]} note>۱۰/۱۰/۹۸</Text>
                                                <Text style={[styles.text,{}]}> -</Text>
                                                <Text style={[styles.text,{}]} note> ۱۳:۳۰</Text>
                                            </Left>
                                            <Body>
                                                <Text style={[styles.text,{fontWeight:'bold'}]}>سید حسین سعیدی</Text>
                                                <Text style={[styles.text,{}]} note>مشاور خانواده</Text>
                                            </Body>
                                            <Right style={[styles.rightStyle,{}]}>
                                                <Avatar
                                                    rounded
                                                    source={require('../img/hs.jpg')}
                                                />
                                            </Right>
                                        </ListItem>
                                    </List>
                                </View>
                            </Tab>
                            <Tab style={[styles.tab,{}]} heading={
                                <TabHeading>
                                    <Text style={{fontSize:17}}>رزرو حضوری</Text>
                                    <Icon name="person-pin" type="MaterialIcons" style={{fontSize:17}}/>
                                </TabHeading>
                            }>
                                <View style={[styles.cardStyle,{}]}>
                                    <List style={[styles.listStyle,{}]}>
                                        <ListItem style={[styles.listItemStyle,{}]}>
                                            <Left>
                                                <Text style={[styles.text,{}]} note>۱۰/۱۰/۹۸</Text>
                                                <Text style={[styles.text,{}]}> -</Text>
                                                <Text style={[styles.text,{}]} note> ۱۳:۳۰</Text>
                                            </Left>
                                            <Body>
                                                <Text style={[styles.text,{fontWeight:'bold'}]}>سید حسین سعیدی</Text>
                                                <Text style={[styles.text,{}]} note>مشاور خانواده</Text>
                                            </Body>
                                            <Right style={[styles.rightStyle,{}]}>
                                                <Avatar
                                                    rounded
                                                    source={require('../img/hs.jpg')}
                                                />
                                            </Right>
                                        </ListItem>
                                        <ListItem style={[styles.listItemStyle,{}]}>
                                            <Left>
                                                <Text style={[styles.text,{}]} note>۱۰/۱۰/۹۸</Text>
                                                <Text style={[styles.text,{}]}> -</Text>
                                                <Text style={[styles.text,{}]} note> ۱۳:۳۰</Text>
                                            </Left>
                                            <Body>
                                                <Text style={[styles.text,{fontWeight:'bold'}]}>سید حسین سعیدی</Text>
                                                <Text style={[styles.text,{}]} note>مشاور خانواده</Text>
                                            </Body>
                                            <Right style={[styles.rightStyle,{}]}>
                                                <Avatar
                                                    rounded
                                                    source={require('../img/hs.jpg')}
                                                />
                                            </Right>
                                        </ListItem>
                                        <ListItem style={[styles.listItemStyle,{}]}>
                                            <Left>
                                                <Text style={[styles.text,{}]} note>۱۰/۱۰/۹۸</Text>
                                                <Text style={[styles.text,{}]}> -</Text>
                                                <Text style={[styles.text,{}]} note> ۱۳:۳۰</Text>
                                            </Left>
                                            <Body>
                                                <Text style={[styles.text,{fontWeight:'bold'}]}>سید حسین سعیدی</Text>
                                                <Text style={[styles.text,{}]} note>مشاور خانواده</Text>
                                            </Body>
                                            <Right style={[styles.rightStyle,{}]}>
                                                <Avatar
                                                    rounded
                                                    source={require('../img/hs.jpg')}
                                                />
                                            </Right>
                                        </ListItem>
                                        <ListItem style={[styles.listItemStyle,{}]}>
                                            <Left>
                                                <Text style={[styles.text,{}]} note>۱۰/۱۰/۹۸</Text>
                                                <Text style={[styles.text,{}]}> -</Text>
                                                <Text style={[styles.text,{}]} note> ۱۳:۳۰</Text>
                                            </Left>
                                            <Body>
                                                <Text style={[styles.text,{fontWeight:'bold'}]}>سید حسین سعیدی</Text>
                                                <Text style={[styles.text,{}]} note>مشاور خانواده</Text>
                                            </Body>
                                            <Right style={[styles.rightStyle,{}]}>
                                                <Avatar
                                                    rounded
                                                    source={require('../img/hs.jpg')}
                                                />
                                            </Right>
                                        </ListItem>
                                    </List>
                                </View>
                            </Tab>
                        </Tabs>
                    </Content>
                    <Footer style={styles.footerStyle}>
                        <FooterTab>
                            <Button vertical onPress={()=>this.props.navigation.navigate('Profile')}>
                                <Image source={require('../img/footer/user.png')}/>
                                <Text style={styles.textStyle}>پروفایل</Text>
                            </Button>
                            <Button vertical onPress={()=>this.props.navigation.navigate('Consultantslist')}>
                                <Image source={require('../img/footer/support.png')}/>
                                <Text style={styles.textStyle}>مشاوران</Text>
                            </Button>
                            <Button vertical onPress={()=>this.props.navigation.navigate('Map')}>
                                <Image source={require('../img/footer/gps.png')}/>
                                <Text style={styles.textStyle}>نقشه</Text>
                            </Button>
                            <Button vertical onPress={()=>this.props.navigation.navigate('Reports')}>
                                <Image source={require('../img/footer/newspaper.png')}/>
                                <Text style={styles.textStyle}>گزارشات</Text>
                            </Button>
                            <Button vertical onPress={()=>this.props.navigation.navigate('Home')}>
                                <Image source={require('../img/footer/home.png')}/>
                                <Text style={styles.textStyle}>خانه</Text>
                            </Button>
                        </FooterTab>
                    </Footer>
                </Container>
            </Drawer>
        );
    }
}

const styles = StyleSheet.create({
    rightStyle:{
        backgroundColor:'transparent',
        paddingRight:4,
        paddingTop:4,
        paddingBottom:4,
        paddingLeft:1,
        borderRadius:50,
        borderWidth: 1,
        borderColor:'#61d6cd'
    },
    footerStyle:{
        backgroundColor:'#3f4392',
        height:65,
    },
    textStyle:{
        color:'#FFF',
        marginTop:3,
        fontSize:13,
    },
    listItemStyle:{
        backgroundColor:'#3f4392',
        marginRight:'3%',
        marginLeft:'3%',
        borderRadius:20,
        paddingLeft:10,
        marginTop:4,
    },
    text:{
        textAlign:'right',
        color:'#61d6cd',
        fontSize:12
    },
    listStyle:{
        paddingVertical:15
    },
    tab:{
        borderColor: 'transparent',
        borderBottomWidth: 0,
        padding:5,
    },
    cardStyle:{
        backgroundColor:'transparent',
        borderRadius:20,
        borderWidth:0,
    },
})

export {ReservationsComponent}
