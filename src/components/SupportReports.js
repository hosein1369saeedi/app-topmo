
import React, {Component} from 'react';
import {StyleSheet,Image,TouchableOpacity,View} from 'react-native';
import {Container,Header, Left, Button,Text, Card, Grid, Right, Form, Footer, FooterTab, Drawer, Icon, List, ListItem, Thumbnail, Col, Picker, Content, Body, Tab, TabHeading, ActionSheet,Switch } from 'native-base';
import {SideBar} from './index';
import Modal from 'react-native-modal';
import {Avatar,Rating,Badge,AirbnbRating} from 'react-native-elements';
import ModalSelector from 'react-native-modal-selector';

class SupportReportsComponent extends Component {
    state = {
        isModalVisible: false,
    };

    _toggleModal = () =>
    this.setState({ isModalVisible: !this.state.isModalVisible });
    
    constructor(props){
        super(props);
        this.state={
            selected: "key0",
            textInputValue: ''
        }
    }
    onValueChange(value: string) {
        this.setState({
            selected: value
        });
    }
    closeDrawer=()=>{
        this.drawer._root.close()
    }
    openDrawer=()=>{
        this.drawer._root.open()
    }
    render() {
        const { rating } = this.props;
        const {navigate}=this.props.navigation;
        return (
            <Drawer
            ref={(ref)=>{this.drawer=ref;}}
            content={<SideBar navigate={navigate}/>}
            onClose={()=>this.closeDrawer()}
            >
                <Container>
                    <Header style={{backgroundColor:'#3f4392'}}>
                        <Left>
                            <Grid>
                                <Col style={{width:'20%'}}>
                                    <Button transparent>
                                        <Icon style={{color:'#FFF'}} name="ios-arrow-back" size={30} type="Ionicons"/>
                                    </Button>
                                </Col>
                                <Col style={{width:'50%'}}>
                                    <TouchableOpacity style={{marginTop:10,marginLeft:10}} onPress={this._toggleModal}>
                                        <Icon name="ios-search" style={{color:'#FFF'}} />
                                    </TouchableOpacity>
                                    <Modal isVisible={this.state.isModalVisible} style={{backgroundColor:'#FFF',borderRadius:20,flex:1}}>
                                        <View style={{ flex: 1 }}>
                                            <Header style={{paddingBottom:10,backgroundColor:'#3f4392',borderTopLeftRadius:20,borderTopRightRadius:20}}>
                                                <Left>
                                                    <TouchableOpacity onPress={this._toggleModal}>
                                                        <Icon style={{color:'#FFF',fontSize:25,marginLeft: 10,}} name="close" type="AntDesign"/>
                                                    </TouchableOpacity>
                                                </Left>
                                                <Right></Right>
                                            </Header>
                                            <View>
                                                <Card style={{paddingBottom:'5%',paddingTop:'5%'}}>
                                                    <Thumbnail source={require('../img/hs.jpg')} style={{marginLeft:'43%',}}/>
                                                    <Text style={{textAlign:'center',fontWeight:'bold',marginTop:15}}>سید حسین سعیدی</Text>
                                                    <Text style={{textAlign:'center',marginTop:5}}>مشاور حقوقی</Text>
                                                </Card>
                                                <Text style={{textAlign:'right',color:'#3f4392',paddingRight: 15,}}>انتخاب نوع مشاوره :</Text>
                                                <Picker
                                                mode="dropdown"
                                                iosHeader="نوع مشاوره"
                                                iosIcon={<Icon name="arrow-down" />}
                                                style={{ width:'100%',borderBottomWidth:1,borderBottomColor:'#999',textAlign:'right'}}
                                                selectedValue={this.state.selected}
                                                onValueChange={this.onValueChange.bind(this)}
                                                >
                                                    <Picker.Item label="انتخاب" value="key0" />
                                                    <Picker.Item label="حضوری" value="key1" />
                                                    <Picker.Item label="تلفنی" value="key2" />
                                                    <Picker.Item label="نوشتاری" value="key3" />
                                                </Picker>
                                                <Text style={{textAlign:'right',color:'#3f4392',paddingRight: 15,}}>انتخاب شهر :</Text>
                                                <Picker
                                                mode="dropdown"
                                                iosHeader="نوع مشاوره"
                                                iosIcon={<Icon name="arrow-down" />}
                                                style={{ width:'100%',borderBottomWidth:1,borderBottomColor:'#999',textAlign:'right'}}
                                                selectedValue={this.state.selected}
                                                onValueChange={this.onValueChange.bind(this)}
                                                >
                                                    <Picker.Item label="انتخاب" value="key0" />
                                                    <Picker.Item label="حضوری" value="key1" />
                                                    <Picker.Item label="تلفنی" value="key2" />
                                                    <Picker.Item label="نوشتاری" value="key3" />
                                                </Picker>
                                                <Text style={{textAlign:'right',color:'#3f4392',paddingRight: 15,}}>انتخاب تخصص مشاور :</Text>
                                                <Picker
                                                mode="dropdown"
                                                iosHeader="نوع مشاوره"
                                                iosIcon={<Icon name="arrow-down" />}
                                                style={{ width:'100%',borderBottomWidth:1,borderBottomColor:'#999',textAlign:'right'}}
                                                selectedValue={this.state.selected}
                                                onValueChange={this.onValueChange.bind(this)}
                                                >
                                                    <Picker.Item label="انتخاب" value="key0" />
                                                    <Picker.Item label="حضوری" value="key1" />
                                                    <Picker.Item label="تلفنی" value="key2" />
                                                    <Picker.Item label="نوشتاری" value="key3" />
                                                </Picker>
                                                <Button full style={{backgroundColor:'#3f4392',fontSize:15,margin:5,borderRadius:10}}>
                                                    <Text>جستجو</Text>
                                                </Button>
                                            </View>                            
                                        </View>
                                    </Modal>
                                </Col>
                            </Grid>
                        </Left>
                        <Right>
                            <Grid>
                                <Col style={{width:'75%'}}>
                                    <Button style={{backgroundColor:'transparent',}} onPress={()=>this.props.navigate('Home')} >
                                        <Image source={require('../img/topmo1.png')}/>
                                    </Button>
                                </Col>
                                <Col style={{width:'25%'}}>
                                    <Button transparent onPress={()=>this.openDrawer()}>
                                        <Icon name="navicon" type="FontAwesome" style={{color:'#FFF'}}/>
                                    </Button>
                                </Col>
                            </Grid>
                        </Right>
                    </Header>
                    <Content>
                        <View style={{borderBottomWidth:2,borderBottomColor:'#3f4392',marginTop:30,marginBottom:25}}></View>
                        <View style={{position:'absolute',marginLeft: '33%',}}>
                            <Text style={{textAlign:'center',backgroundColor:'#FFF',marginTop:18,color:'#3f4392',fontWeight:'bold',}}>گزارشات پشتیبانی</Text>
                        </View>
                        <View style={[styles.viewStyle,{}]}>
                            <Text style={[styles.text,{fontWeight:'bold'}]}>
                                ساعت و تاریخ ارسال : 
                                <Text style={[styles.text,{fontWeight:'100'}]}>۰۴:۰۰:۱۲</Text> 
                                <Text style={[styles.text,{fontWeight:'100'}]}>۱۰/۰۲/۱۳۹۸</Text>
                            </Text>
                            <Text style={[styles.text,{fontWeight:'bold'}]}>
                                عنوان : 
                                <Text style={[styles.text,{fontWeight:'100'}]}>اشکال بروزرسانی کیف پول</Text>
                            </Text>
                            <Text style={[styles.text,{fontWeight:'bold'}]}>
                                دریافت کننده : 
                                <Text style={[styles.text,{fontWeight:'100'}]}>مالی</Text>
                            </Text>
                            <Text style={[styles.text,{fontWeight:'bold'}]}>
                                شرح : 
                                <Text style={[styles.text,{fontWeight:'100'}]}>افزایش اعتبار داشتم,اما کیف پول بروزرسانی نشد</Text>
                            </Text>
                            <Text style={[styles.text,{fontWeight:'bold'}]}>
                                وضعیت : 
                                <Text style={[styles.text,{color:'red',fontWeight:'100'}]}>در دست اقدام</Text>
                            </Text>
                        </View>
                        <View style={[styles.viewStyle,{}]}>
                            <Text style={[styles.text,{fontWeight:'bold'}]}>
                                ساعت و تاریخ ارسال : 
                                <Text style={[styles.text,{fontWeight:'100'}]}>۰۴:۰۰:۱۲</Text> 
                                <Text style={[styles.text,{fontWeight:'100'}]}>۱۰/۰۲/۱۳۹۸</Text>
                            </Text>
                            <Text style={[styles.text,{fontWeight:'bold'}]}>
                                عنوان : 
                                <Text style={[styles.text,{fontWeight:'100'}]}>اشکال بروزرسانی کیف پول</Text>
                            </Text>
                            <Text style={[styles.text,{fontWeight:'bold'}]}>
                                دریافت کننده : 
                                <Text style={[styles.text,{fontWeight:'100'}]}>مالی</Text>
                            </Text>
                            <Text style={[styles.text,{fontWeight:'bold'}]}>
                                شرح : 
                                <Text style={[styles.text,{fontWeight:'100'}]}>افزایش اعتبار داشتم,اما کیف پول بروزرسانی نشد</Text>
                            </Text>
                            <Text style={[styles.text,{fontWeight:'bold'}]}>
                                وضعیت : 
                                <Text style={[styles.text,{color:'red',fontWeight:'100'}]}>در دست اقدام</Text>
                            </Text>
                        </View>
                        <View style={[styles.viewStyle,{}]}>
                            <Text style={[styles.text,{fontWeight:'bold'}]}>
                                ساعت و تاریخ ارسال : 
                                <Text style={[styles.text,{fontWeight:'100'}]}>۰۴:۰۰:۱۲</Text> 
                                <Text style={[styles.text,{fontWeight:'100'}]}>۱۰/۰۲/۱۳۹۸</Text>
                            </Text>
                            <Text style={[styles.text,{fontWeight:'bold'}]}>
                                عنوان : 
                                <Text style={[styles.text,{fontWeight:'100'}]}>اشکال بروزرسانی کیف پول</Text>
                            </Text>
                            <Text style={[styles.text,{fontWeight:'bold'}]}>
                                دریافت کننده : 
                                <Text style={[styles.text,{fontWeight:'100'}]}>مالی</Text>
                            </Text>
                            <Text style={[styles.text,{fontWeight:'bold'}]}>
                                شرح : 
                                <Text style={[styles.text,{fontWeight:'100'}]}>افزایش اعتبار داشتم,اما کیف پول بروزرسانی نشد</Text>
                            </Text>
                            <Text style={[styles.text,{fontWeight:'bold'}]}>
                                وضعیت : 
                                <Text style={[styles.text,{color:'red',fontWeight:'100'}]}>در دست اقدام</Text>
                            </Text>
                        </View>
                    </Content>
                    <Footer style={styles.footerStyle}>
                        <FooterTab>
                            <Button vertical onPress={()=>this.props.navigation.navigate('Profile')}>
                                <Image source={require('../img/footer/user.png')}/>
                                <Text style={styles.textStyle}>پروفایل</Text>
                            </Button>
                            <Button vertical onPress={()=>this.props.navigation.navigate('Consultantslist')}>
                                <Image source={require('../img/footer/support.png')}/>
                                <Text style={styles.textStyle}>مشاوران</Text>
                            </Button>
                            <Button vertical onPress={()=>this.props.navigation.navigate('Map')}>
                                <Image source={require('../img/footer/gps.png')}/>
                                <Text style={styles.textStyle}>نقشه</Text>
                            </Button>
                            <Button vertical onPress={()=>this.props.navigation.navigate('Reports')}>
                                <Image source={require('../img/footer/newspaper.png')}/>
                                <Text style={styles.textStyle}>گزارشات</Text>
                            </Button>
                            <Button vertical onPress={()=>this.props.navigation.navigate('Home')}>
                                <Image source={require('../img/footer/home.png')}/>
                                <Text style={styles.textStyle}>خانه</Text>
                            </Button>
                        </FooterTab>
                    </Footer>
                </Container>
            </Drawer>
        );
    }
}

const styles = StyleSheet.create({
    footerStyle:{
        backgroundColor:'#3f4392',
        height:65,
    },
    textStyle:{
        color:'#FFF',
        marginTop:3,
        fontSize:13,
    },
    viewStyle:{
        backgroundColor:'#61d6cd',
        margin:'2%',
        borderRadius:10,
        padding:15,
        shadowColor: '#3f4392',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.8,
        shadowRadius: 2,
    },
    text:{
        textAlign:'right',
        color:'#3f4392',
        fontSize:15,
        marginTop:0,
    },
    rightStyle:{
        backgroundColor:'transparent',
        paddingRight:6,
        paddingTop:5,
        paddingBottom:5,
        borderRadius:50,
        borderWidth: 1,
        borderColor:'#61d6cd',
    },
})

export {SupportReportsComponent}
