import React, {Component} from 'react';
import{ StyleSheet, Image} from 'react-native';
import{ Container, Content, List, ListItem, Left, Body, Right, Button, Text, Icon, View, Row, Col, Grid} from 'native-base';
import { Avatar, Badge } from 'react-native-elements';
class SideBar extends Component {
    constructor(props){
         super(props) 
    }
    render(){    
        return(
            <Container style={styles.SideBarStyle}>
                <Content>
                    <View style={{backgroundColor:'#3f4392',height:190}}>
                        <Left style={styles.userStyle}>
                            <View>
                                <Avatar
                                    rounded
                                    source={require('../img/user.png')}
                                    size="large"
                                />
                                <Badge
                                    status="success"
                                    Style={{ position: 'absolute', top: -30, marginRight: 20 }}
                                />
                            </View>
                        </Left>
                        <Body></Body>
                        <Right style={styles.logoStyle}>
                            <Button style={{backgroundColor:'transparent',marginTop:'-50%',marginRight:'70%'}} onPress={()=>this.props.navigate('Home')} >
                                <Image source={require('../img/topmo1.png')}/>
                            </Button>
                        </Right>
                        <View style={styles.viewStyle}>
                            <Text style={styles.text}>سید حسین سعیدی</Text>
                            <Text style={styles.textStyle}>مشاور / کاربر</Text>
                            <Text style={styles.textStyle}>شماره موبایل : ۰۹۱۲۳۴۵۶۷۸۹</Text>
                            <Text style={styles.textStyle}>شارژ : ۱۰۰/۰۰۰ تومان</Text>
                        </View>
                    </View>
                    <List style={{direction:'rtl'}}>
                        <ListItem>
                            <Button style={[styles.buttonStyle,{}]} onPress={()=>this.props.navigate('ReportsCall')}>
                                <Icon style={[styles.Icon,{}]} name="phone-square" type="FontAwesome" />
                                <Text style={styles.sideButton}>گزارش تماسها</Text>
                                <Badge status="error" />
                            </Button>
                        </ListItem>
                        <ListItem>
                            <Button style={[styles.buttonStyle,{}]} onPress={()=>this.props.navigate('ReportsReservation')}>
                                <Icon style={[styles.Icon,{}]} name="clipboard-check-outline" type="MaterialCommunityIcons" />
                                <Text style={styles.sideButton}>گزارش رزروها</Text>
                                <Badge status="error" />
                            </Button>
                        </ListItem>
                        <ListItem>
                            <Button style={[styles.buttonStyle,{}]} onPress={()=>this.props.navigate('Aboutus')}>
                                <Icon style={[styles.Icon,{}]} name="exclamationcircle" type="AntDesign"/>
                                <Text style={styles.sideButton}>درباره ما</Text>
                            </Button>
                        </ListItem>
                        <ListItem>
                            <Button style={[styles.buttonStyle,{}]} onPress={()=>this.props.navigate('Setting')}>
                                <Icon style={[styles.Icon,{}]} name="settings" type="MaterialIcons"/>
                                <Text style={styles.sideButton}>تنظیمات</Text>
                            </Button>
                        </ListItem>
                        <ListItem>
                            <Button style={[styles.buttonStyle,{}]} onPress={()=>this.props.navigate('Register')}>
                                <Icon style={[styles.Icon,{}]} name="add-user" type="Entypo"/> 
                                <Text style={styles.sideButton}>ثبت نام</Text>

                            </Button>
                        </ListItem>
                        <ListItem>
                            <Button style={[styles.buttonStyle,{}]} onPress={()=>this.props.navigate('Login')}>
                                <Icon style={[styles.Icon,{}]} name="login" type="AntDesign"/>
                                <Text style={styles.sideButton}>ورود</Text>
                            </Button>
                        </ListItem>
                    </List>
                </Content>
            </Container>
        )
    }
}
const styles = StyleSheet.create({
    logoStyle:{
        width:'10%',
        marginTop:'-60%',
        marginRight:'35%'
    },
    userStyle:{
        marginTop:'33%',
        marginLeft:'65%'
    },
    sideButton:{
        fontSize:13,
        color:'#414a95',
        
    },
    viewStyle:{
        padding:7,
        marginRight:'30%',
    },
    text:{
        fontWeight:'bold',
        fontSize:15,
        textAlign:'right',
        color:'#61D6CD',
    },
    textStyle:{
        textAlign:'right',
        color:'#FFF',
        fontSize:12,
    },
    SideBarStyle:{
        flex:1,
        backgroundColor:'#EEE',
    },
    buttonStyle:{
        backgroundColor:'#eee',
    },
    imageStyle:{
        marginLeft:3.5,
    },
    Icon:{
        color:'#3f4392'
    },
    badgeStyle:{
        backgroundColor:'#3f4392',
        borderWidth:0,
        marginTop:-10
    }
})
export {SideBar}