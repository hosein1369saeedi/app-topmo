
import React, {Component} from 'react';
import {StyleSheet,Image,View} from 'react-native';
import {Container,Header, Left, Button,Text, Card, Grid, Right, Item, Footer, FooterTab, Drawer, Icon, List, ListItem, Thumbnail, Col, Content, Body, Input } from 'native-base';
import {SideBar} from './index';
import Modal from 'react-native-modal';
import {Avatar,Badge,} from 'react-native-elements';
import ModalSelector from 'react-native-modal-selector';

class OnlinPersonListComponent extends Component {
    state = {
        isModalVisible: false,
    }
    toggleModal = () => {
        this.setState({ isModalVisible: !this.state.isModalVisible });
    }
    constructor(props){
        super(props);
        this.state={
            selected: "key0",
            textInputValue: ''
        }
    }
    onValueChange(value: string) {
        this.setState({
            selected: value
        });
    }
    closeDrawer=()=>{
        this.drawer._root.close()
    }
    openDrawer=()=>{
        this.drawer._root.open()
    }
    render() {
        const { rating } = this.props;
        let index = 0;
        const data = [
            { key: index++, label: 'مشاوره خانواده'},
            { key: index++, label: 'مشاوره ازدواج'},
            { key: index++, label: 'مشاوره جنسی'},
            { key: index++, label: 'مشاوره فردی'},
            { key: index++, label: 'مشاوره افسردگی'},
            { key: index++, label: 'مشاوره کودک و نوجوان'},
            { key: index++, label: 'مشاوره روانشناسی آنلاین '}
        ];
        const data2 = [
            { key: index++, label: 'مشاوره راه اندازی کسب و کار آنلاین'},
            { key: index++, label: 'مشاوره انتخاب رشته تحصیلی'},
            { key: index++, label: 'مشاوره استخدام'},
            { key: index++, label: 'مشاوره کسب و کار اینترنتی'},
            { key: index++, label: 'مشاوره اینستاگرام'},
            { key: index++, label: 'مشاوره تلگرام'}
        ];
        const data3 = [
            { key: index++, label: 'مشاوره اعتیاد رفتاری'},
            { key: index++, label: 'مشاوره ترک اعتیاد'},
            { key: index++, label: 'مشاوره اعتیاد کودک و نوجوان'},
        ];
        const {navigate}=this.props.navigation;
        return (
            <Drawer
                ref={(ref)=>{this.drawer=ref;}}
                content={<SideBar navigate={navigate}/>}
                onClose={()=>this.closeDrawer()}
            >
                <Container>
                    <Header style={{backgroundColor:'#3f4392'}}>
                        <Left>
                            <Grid>
                                <Col style={{width:'20%'}}>
                                    <Button transparent onPress={()=>this.props.navigation.goBack()}>
                                        <Icon style={{color:'#FFF'}} name="ios-arrow-back" size={30} type="Ionicons"/>
                                    </Button>
                                </Col>
                                <Col style={{width:'50%'}}>
                                    <Button transparent title="Show modal" onPress={this.toggleModal} >
                                        <Icon name="search" type="FontAwesome" style={{color:'#FFF',fontSize:30}} />
                                    </Button>
                                    <Modal isVisible={this.state.isModalVisible}>
                                        <View style={{ flex: 1,paddingTop:'30%',}}>
                                            <View style={{backgroundColor:'#FFF',borderRadius:20}}>
                                                <Button
                                                    style={{
                                                        backgroundColor:'#3f4392',
                                                        borderRadius:20,
                                                        borderBottomLeftRadius:0,
                                                        borderBottomRightRadius:0,
                                                    }}
                                                    title="Hide modal" onPress={this.toggleModal} 
                                                >
                                                    <Icon style={{color:'#FFF',fontSize:25,marginLeft: 10,}} name="close" type="AntDesign"/>
                                                </Button>
                                                <Card style={{paddingBottom:'5%',paddingTop:'5%'}}>
                                                    <Thumbnail source={require('../img/hs.jpg')} style={{marginLeft:'43%',}}/>
                                                    <Text style={{textAlign:'center',fontWeight:'bold',marginTop:15}}>سید حسین سعیدی</Text>
                                                    <Text style={{textAlign:'center',marginTop:5}}>مشاور حقوقی</Text>
                                                </Card>
                                                <ModalSelector
                                                    data={data}
                                                    initValue="انتخاب نوع مشاوره"
                                                    style={[styles.ModalSelector]}
                                                />
                                                <ModalSelector
                                                    data={data2}
                                                    initValue="انتخاب شهر مشاور"
                                                    style={[styles.ModalSelector]}
                                                />
                                                <ModalSelector
                                                    data={data3}
                                                    initValue="انتخاب تخصص مشاور"
                                                    style={[styles.ModalSelector]}
                                                />
                                                <Button full style={{backgroundColor:'#3f4392',fontSize:15,margin:5,borderBottomLeftRadius:20,borderBottomEndRadius:20}}>
                                                    <Text>جستجو</Text>
                                                </Button>
                                            </View>  
                                        </View>
                                    </Modal>
                                </Col>
                            </Grid>
                        </Left>
                        <Right>
                            <Grid>
                                <Col style={{width:'75%'}}>
                                    <Button style={{backgroundColor:'transparent',}} onPress={()=>this.props.navigate('Home')} >
                                        <Image source={require('../img/topmo1.png')}/>
                                    </Button>
                                </Col>
                                <Col style={{width:'25%'}}>
                                    <Button transparent onPress={()=>this.openDrawer()}>
                                        <Icon name="navicon" type="FontAwesome" style={{color:'#FFF'}}/>
                                    </Button>
                                </Col>
                            </Grid>
                        </Right>
                    </Header>
                    <Content>
                        <View style={{borderBottomWidth:2,borderBottomColor:'#3f4392',marginTop:30,marginBottom:25}}></View>
                        <View style={{position:'absolute',marginLeft: '33%',}}>
                            <Text style={{textAlign:'center',backgroundColor:'#FFF',marginTop:18,color:'#3f4392',fontWeight:'bold',}}>مشاوران آنلاین</Text>
                        </View>
                        <View style={[,{}]}>
                            <List style={[styles.listStyle,{}]}>
                                <ListItem style={[styles.listItemStyle,{backgroundColor:'#61d6cd'}]}>
                                    <Item>
                                        <Input style={[{paddingRight:'10%',textAlign:'right'}]} placeholderTextColor={'#3f4392'} placeholder="نام مشاور مورد نظر را بنویسید..."/>
                                    </Item>
                                    <Button transparent style={{position:'absolute',right:0,top:20}}>
                                        <Icon style={[{color:'#3f4392'}]} name="ios-search" />
                                    </Button>
                                </ListItem>
                                <ListItem style={[styles.listItemStyle,{}]}>
                                    <Left>
                                        <Button transparent onPress={()=>this.props.navigation.navigate('Consultantdetails')}>
                                            <Icon name="leftcircle" type="AntDesign" style={{color:'#61d6cd',fontSize:30}}/>
                                        </Button>
                                    </Left>
                                    <Body>
                                        <Text style={[styles.text,{fontWeight:'bold'}]}>سید حسین سعیدی</Text>
                                        <Text style={[styles.text,{}]} note>مشاور خانواده</Text>
                                    </Body>
                                    <Right style={[styles.rightStyle,{}]}>
                                        <Avatar
                                            rounded
                                            source={require('../img/user.png')}
                                        />
                                        <Badge
                                            status="success"
                                            containerStyle={{ position: 'absolute', top: 0, right: -1 }}
                                        />
                                    </Right>
                                </ListItem>
                                <ListItem style={[styles.listItemStyle,{}]}>
                                    <Left>
                                        <Button transparent onPress={()=>this.props.navigation.navigate('Consultantdetails')}>
                                            <Icon name="leftcircle" type="AntDesign" style={{color:'#61d6cd',fontSize:30}}/>
                                        </Button>
                                    </Left>
                                    <Body>
                                        <Text style={[styles.text,{fontWeight:'bold'}]}>سید حسین سعیدی</Text>
                                        <Text style={[styles.text,{}]} note>مشاور خانواده</Text>
                                    </Body>
                                    <Right style={[styles.rightStyle,{}]}>
                                        <Avatar
                                            rounded
                                            source={require('../img/user.png')}
                                        />
                                        <Badge
                                            status="success"
                                            containerStyle={{ position: 'absolute', top: 0, right: -1 }}
                                        />
                                    </Right>
                                </ListItem>
                                <ListItem style={[styles.listItemStyle,{}]}>
                                    <Left>
                                        <Button transparent onPress={()=>this.props.navigation.navigate('Consultantdetails')}>
                                            <Icon name="leftcircle" type="AntDesign" style={{color:'#61d6cd',fontSize:30}}/>
                                        </Button>
                                    </Left>
                                    <Body>
                                        <Text style={[styles.text,{fontWeight:'bold'}]}>سید حسین سعیدی</Text>
                                        <Text style={[styles.text,{}]} note>مشاور خانواده</Text>
                                    </Body>
                                    <Right style={[styles.rightStyle,{}]}>
                                        <Avatar
                                            rounded
                                            source={require('../img/user.png')}
                                        />
                                        <Badge
                                            status="success"
                                            containerStyle={{ position: 'absolute', top: 0, right: -1 }}
                                        />
                                    </Right>
                                </ListItem>
                                <ListItem style={[styles.listItemStyle,{}]}>
                                    <Left>
                                        <Button transparent onPress={()=>this.props.navigation.navigate('Consultantdetails')}>
                                            <Icon name="leftcircle" type="AntDesign" style={{color:'#61d6cd',fontSize:30}}/>
                                        </Button>
                                    </Left>
                                    <Body>
                                        <Text style={[styles.text,{fontWeight:'bold'}]}>سید حسین سعیدی</Text>
                                        <Text style={[styles.text,{}]} note>مشاور خانواده</Text>
                                    </Body>
                                    <Right style={[styles.rightStyle,{}]}>
                                        <Avatar
                                            rounded
                                            source={require('../img/user.png')}
                                        />
                                        <Badge
                                            status="success"
                                            containerStyle={{ position: 'absolute', top: 0, right: -1 }}
                                        />
                                    </Right>
                                </ListItem>
                            </List>
                        </View>
                    </Content>
                    <Footer style={styles.footerStyle}>
                        <FooterTab>
                            <Button vertical onPress={()=>this.props.navigation.navigate('Profile')}>
                                <Icon name="user" type="Entypo" style={{color:'#fff',fontSize:30}}/>
                                <Text style={styles.textStyle}>پروفایل</Text>
                            </Button>
                            <Button vertical onPress={()=>this.props.navigation.navigate('Consultantslist')}>
                                <Icon name="perm-contact-calendar" type="MaterialIcons" style={{color:'#fff',fontSize:30}}/>
                                <Text style={styles.textStyle}>مشاوران</Text>
                            </Button>
                            <Button vertical onPress={()=>this.props.navigation.navigate('Map')}>
                                <Icon name="map-marked-alt" type="FontAwesome5" style={{color:'#fff',fontSize:30}}/>
                                <Text style={styles.textStyle}>نقشه</Text>
                            </Button>
                            <Button vertical onPress={()=>this.props.navigation.navigate('Reports')}>
                                <Icon name="md-list-box" type="Ionicons" style={{color:'#fff',fontSize:30}}/>
                                <Text style={styles.textStyle}>گزارشات</Text>
                            </Button>
                            <Button vertical onPress={()=>this.props.navigation.navigate('Home')}>
                                <Icon name="home" type="FontAwesome" style={{color:'#fff',fontSize:30}}/>
                                <Text style={styles.textStyle}>خانه</Text>
                            </Button>
                        </FooterTab>
                    </Footer>
                </Container>
            </Drawer>
        );
    }
}

const styles = StyleSheet.create({
    footerStyle:{
        backgroundColor:'#3f4392',
        height:65,
    },
    textStyle:{
        color:'#FFF',
        marginTop:3,
        fontSize:13,
    },
    listStyle:{
        paddingVertical:15
    },
    listItemStyle:{
        backgroundColor:'#3f4392',
        marginRight:'3%',
        marginLeft:'3%',
        borderRadius:25,
        paddingLeft:10,
        marginTop:5,
    },
    text:{
        textAlign:'right',
        color:'#61d6cd',
        fontSize:12,
        marginRight:22,
        marginTop:0
    },
    rightStyle:{
        backgroundColor:'transparent',
        paddingRight:6,
        paddingTop:5,
        paddingBottom:5,
        borderRadius:50,
        borderWidth: 1,
        borderColor:'#61d6cd',
    },
})

export {OnlinPersonListComponent}
