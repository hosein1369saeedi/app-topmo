
import React, {Component} from 'react';
import {StyleSheet,Image,TouchableOpacity,View} from 'react-native';
import {Container,Header, Left, Button,Text, Card, Grid, Right, Body, Footer, FooterTab, Drawer, Icon, Item, Input, Thumbnail, Col, Picker, Content, Tabs, Tab, TabHeading, ActionSheet,Switch, List } from 'native-base';
import {SideBar} from './index';
import Modal from 'react-native-modal';
import ModalSelector from 'react-native-modal-selector';
import { ListItem } from 'react-native-elements';

class ServicesComponent extends Component {
    state = {
        isModalVisible: false,
    };

    _toggleModal = () =>
    this.setState({ isModalVisible: !this.state.isModalVisible });
    
    constructor(props){
        super(props);
        this.state={
            selected: "key0",
            textInputValue: ''
        }
    }
    onValueChange(value: string) {
        this.setState({
            selected: value
        });
    }
    closeDrawer=()=>{
        this.drawer._root.close()
    }
    openDrawer=()=>{
        this.drawer._root.open()
    }
    render() {
        let index = 0;
        const data = [
          { key: index++, label: 'تلفنی'},
          { key: index++, label: 'نوشتاری'},
          { key: index++, label: 'حضوری'}
        ];
        const data2 = [
          { key: index++, label: 'تهران'},
          { key: index++, label: 'مشهد'},
          { key: index++, label: 'شیراز'},
          { key: index++, label: 'اصفهان'},
          { key: index++, label: 'تبریز'}
        ];
        const data3 = [
          { key: index++, label: 'مشاور خانواده'},
          { key: index++, label: 'مشاور ازدواج'},
          { key: index++, label: 'مشاور کاری'},
        ];
        const {navigate}=this.props.navigation;
        return (
            <Drawer
            ref={(ref)=>{this.drawer=ref;}}
            content={<SideBar navigate={navigate}/>}
            onClose={()=>this.closeDrawer()}
            >
                <Container>
                    <Header style={{backgroundColor:'#3f4392'}}>
                        <Left>
                            <Grid>
                                <Col style={{width:'20%'}}>
                                    <Button transparent>
                                        <Icon style={{color:'#FFF'}} name="ios-arrow-back" size={30} type="Ionicons"/>
                                    </Button>
                                </Col>
                                <Col style={{width:'50%'}}>
                                    <Button transparent title="Show modal" onPress={this.toggleModal} >
                                        <Icon name="ios-search" style={{color:'#FFF'}} />
                                    </Button>
                                    <Modal isVisible={this.state.isModalVisible}>
                                        <View style={{ flex: 1,paddingTop:'30%',}}>
                                        <View style={{backgroundColor:'#FFF',borderRadius:20}}>
                                            <Button
                                            style={{
                                                backgroundColor:'#3f4392',
                                                borderRadius:20,
                                                borderBottomLeftRadius:0,
                                                borderBottomRightRadius:0,
                                            }}
                                            title="Hide modal" onPress={this.toggleModal} 
                                            >
                                            <Icon style={{color:'#FFF',fontSize:25,marginLeft: 10,}} name="close" type="AntDesign"/>
                                            </Button>
                                            <Card style={{paddingBottom:'5%',paddingTop:'5%'}}>
                                            <Thumbnail source={require('../img/hs.jpg')} style={{marginLeft:'43%',}}/>
                                            <Text style={{textAlign:'center',fontWeight:'bold',marginTop:15}}>سید حسین سعیدی</Text>
                                            <Text style={{textAlign:'center',marginTop:5}}>مشاور حقوقی</Text>
                                            </Card>
                                            <ModalSelector
                                            data={data}
                                            initValue="انتخاب نوع مشاوره"
                                            style={[styles.ModalSelector]}
                                            />
                                            <ModalSelector
                                            data={data2}
                                            initValue="انتخاب شهر مشاور"
                                            style={[styles.ModalSelector]}
                                            />
                                            <ModalSelector
                                            data={data3}
                                            initValue="انتخاب تخصص مشاور"
                                            style={[styles.ModalSelector]}
                                            />
                                            <Button full style={{backgroundColor:'#3f4392',fontSize:15,margin:5,borderBottomLeftRadius:20,borderBottomEndRadius:20}}>
                                            <Text>جستجو</Text>
                                            </Button>
                                        </View>  
                                        </View>
                                    </Modal>
                                </Col>
                            </Grid>
                        </Left>
                        <Right>
                            <Grid>
                                <Col style={{width:'75%'}}>
                                    <Button style={{backgroundColor:'transparent',}} onPress={()=>this.props.navigate('Home')} >
                                        <Image source={require('../img/topmo1.png')}/>
                                    </Button>
                                </Col>
                                <Col style={{width:'25%'}}>
                                    <Button transparent onPress={()=>this.openDrawer()}>
                                        <Icon name="navicon" type="FontAwesome" style={{color:'#FFF'}}/>
                                    </Button>
                                </Col>
                            </Grid>
                        </Right>
                    </Header>
                    <Content>
                        <View style={{borderBottomWidth:2,borderBottomColor:'#3f4392',marginTop:30,marginBottom:25}}></View>
                        <View style={{position:'absolute',marginLeft: '27%',}}>
                            <Text style={{textAlign:'center',backgroundColor:'#FFF',marginTop:18,color:'#3f4392',fontWeight:'bold',}}>خدمات مشاوره</Text>
                        </View>
                        <View>
                            <Card style={{padding:10,borderWidth:0}}>
                                <View style={[styles.ViewStyle,{}]}>
                                    <Grid>
                                        <Col style={[styles.ColStyle,{}]}>
                                            <Text style={{textAlign:'right',fontWeight:'bold',color:'#3f4392',}}> امکان رزرو نوبت مشاوره </Text>
                                        </Col>
                                        <Col style={{width:'7%'}}>
                                            <Icon style={[{color:'#ddd',fontSize:25,backgroundColor:'#3f4392'}]} name="hand-pointer-o" type="FontAwesome"/>
                                        </Col>
                                    </Grid>
                                    <Text style={[styles.CardTextStyle,{}]}>
                                        شما می توانید در هر زمانی از روز و از هر جایی که هستید، روز و ساعتی که تمایل دارید را برای مشاوره حضوری، تلفنی و نوشتاری رزرو کنید. 
                                    </Text>
                                </View>
                                <View style={[styles.ViewStyle,{}]}>
                                    <Grid>
                                        <Col style={[styles.ColStyle,{}]}>
                                            <Text style={{textAlign:'right',fontWeight:'bold',color:'#3f4392'}}> پرداخت و شارژ کیف پول </Text>
                                        </Col>
                                        <Col style={{width:'7%'}}>
                                            <Icon style={[{color:'#ddd',fontSize:25,backgroundColor:'#3f4392'}]} name="wallet" type="Entypo"/>
                                        </Col>
                                    </Grid>
                                    <Text style={[styles.CardTextStyle,{}]}>
                                        لازم است پیش از انجام مشاوره کیف پول خود را شارژ کنید و سپس می توانید بر اساس اعتبار خود، وقت مشاوره تان را رزرو کرده و یا به صورت آنلاین مشاوره دریافت کنید. 
                                    </Text>
                                </View>
                                <View style={[styles.ViewStyle,{}]}>
                                    <Grid>
                                        <Col style={[styles.ColStyle,{}]}>
                                            <Text style={{textAlign:'right',fontWeight:'bold',color:'#3f4392'}}> نمایش مشاوران نزدیک من  </Text>
                                        </Col>
                                        <Col style={{width:'7%'}}>
                                            <Icon style={[{color:'#ddd',fontSize:25,backgroundColor:'#3f4392'}]} name="person-pin-circle" type="MaterialIcons"/>
                                        </Col>
                                    </Grid>
                                    <Text style={[styles.CardTextStyle,{}]}>
                                        تاپمو این امکان را به شما می دهد که با مشاوران نزدیک خود آشنا شوید و در صورت تمایل از آنها مشاوره حضوری و یا غیر حضوری دریافت کنید.
                                    </Text>
                                </View>
                                <View style={[styles.ViewStyle,{}]}>
                                    <Grid>
                                        <Col style={[styles.ColStyle,{}]}>
                                            <Text style={{textAlign:'right',fontWeight:'bold',color:'#3f4392'}}> نمایش مشاوران آنلاین  </Text>
                                        </Col>
                                        <Col style={{width:'7%'}}>
                                            <Icon style={[{color:'#ddd',fontSize:25,backgroundColor:'#3f4392'}]} name="wifi" type="AntDesign"/>
                                        </Col>
                                    </Grid>
                                    <Text style={[styles.CardTextStyle,{}]}>
                                    مشاوران آنلاین تاپمو در مواقع اضطراری یا هر زمانی که نیاز به مشاوره داشته باشید در کنار شما هستند و می توانید با فرستادن درخواست مشاوره آنلاین با هر مشاوری که مایل هستید در ارتباط قرار گیرید.
                                    </Text>
                                </View>
                                <View style={[styles.ViewStyle,{}]}>
                                    <Grid>
                                        <Col style={[styles.ColStyle,{}]}>
                                            <Text style={{textAlign:'right',fontWeight:'bold',color:'#3f4392'}}> پروفایل مشاوران  </Text>
                                        </Col>
                                        <Col style={{width:'7%'}}>
                                            <Icon style={[{color:'#ddd',fontSize:20,backgroundColor:'#3f4392'}]} name="user-cog" type="FontAwesome5"/>
                                        </Col>
                                    </Grid>
                                    <Text style={[styles.CardTextStyle,{}]}>
                                        مشاوران تاپمو در پنل مدیریتی خود از امکانات زیادی برخوردارند، از جمله رزرو نوبت های کاری، مشاهده نوبت های رزرو شده از سوی کاربران و درخواست تسویه از سیستم.
                                    </Text>
                                </View>
                                <View style={[styles.ViewStyle,{}]}>
                                    <Grid>
                                        <Col style={{width:'93%',paddingTop:5}}>
                                            <Text style={{textAlign:'right',fontWeight:'bold',color:'#3f4392'}}> پروفایل کاربران  </Text>
                                        </Col>
                                        <Col style={{width:'7%'}}>
                                            <Icon style={[{color:'#ddd',fontSize:20,backgroundColor:'#3f4392'}]} name="user-alt" type="FontAwesome5"/>
                                        </Col>
                                    </Grid>
                                    <Text style={[styles.CardTextStyle,{}]}>
                                        کاربران تاپمو می توانند به راحتی از امکانات پنل کاربری خود استفاده کنند، به سوابق مشاوره ها و آزمون های خود دسترسی داشته باشند و همچنین فایل ها و مدارک لازم را پیش از مشاوره در پروفایل پنل خود قرار دهند. 
                                    </Text>
                                </View>
                            </Card>
                        </View>
                    </Content>
                    <Footer style={[{backgroundColor:'#3f4392',height:60,}]}>
                        <FooterTab>
                            <Button vertical onPress={()=>this.props.navigation.navigate('Profile')}>
                                <Icon name="user" type="Entypo" style={{color:'#fff',fontSize:30}}/>
                                <Text style={styles.textStyle}>پروفایل</Text>
                            </Button>
                            <Button vertical onPress={()=>this.props.navigation.navigate('Consultantslist')}>
                                <Icon name="perm-contact-calendar" type="MaterialIcons" style={{color:'#fff',fontSize:30}}/>
                                <Text style={styles.textStyle}>مشاوران</Text>
                            </Button>
                            <Button vertical onPress={()=>this.props.navigation.navigate('Map')}>
                                <Icon name="map-marked-alt" type="FontAwesome5" style={{color:'#fff',fontSize:30}}/>
                                <Text style={styles.textStyle}>نقشه</Text>
                            </Button>
                            <Button vertical onPress={()=>this.props.navigation.navigate('Reports')}>
                                <Icon name="md-list-box" type="Ionicons" style={{color:'#fff',fontSize:30}}/>
                                <Text style={styles.textStyle}>گزارشات</Text>
                            </Button>
                            <Button vertical onPress={()=>this.props.navigation.navigate('Home')}>
                                <Icon name="home" type="FontAwesome" style={{color:'#fff',fontSize:30}}/>
                                <Text style={styles.textStyle}>خانه</Text>
                            </Button>
                        </FooterTab>
                    </Footer>
                </Container>
            </Drawer>
        );
    }
}

const styles = StyleSheet.create({
    footerStyle:{
        backgroundColor:'#3f4392',
        height:65,
    },
    textStyle:{
        color:'#FFF',
        marginTop:3,
        fontSize:13,
    },
    buttonStyle:{
        backgroundColor:'#61d6cd',
        margin:2,
        borderRadius: 5,
        borderColor: '#ddd',
        borderBottomWidth: 0,
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.3,
        shadowRadius: 2,
        elevation: 1,
        marginLeft: 5,
        marginRight: 5,
        marginTop: 10,
    },
    Text:{
        color:'#3f4392',
        fontWeight: 'bold',
    },
    CardTextStyle:{
        textAlign:'right',
        color:'#3f4392',
        fontSize:15,
    },
    ViewStyle:{
        backgroundColor:'#ddd',
        padding:'2%',
        marginTop:5,
        borderRadius:10
    },
    ColStyle:{
        width:'93%',
        paddingTop:5,
    },
})

export {ServicesComponent}
