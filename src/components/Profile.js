import React, { Component } from 'react';
import { Image,View,StyleSheet } from 'react-native';
import { Container,Header, Left, Button, Right, Content, Drawer, Icon, Grid, Col, Footer, FooterTab, Text, Card, Thumbnail, Body } from 'native-base';
import {SideBar} from '.';
import Modal from 'react-native-modal';
import ModalSelector from 'react-native-modal-selector';

class ProfileComponent extends Component {
    state = {
        isModalVisible: false,
    }
    toggleModal = () => {
        this.setState({ isModalVisible: !this.state.isModalVisible });
    }
    constructor(props){
        super(props);
        this.state={
            selected: "key0",
        }
    }
    onValueChange(value: string) {
        this.setState({
            selected: value
        });
    }
    closeDrawer=()=>{
        this.drawer._root.close()
    }
    openDrawer=()=>{
        this.drawer._root.open()
    }
    render() {
        let index = 0;
        const data = [
          { key: index++, label: 'تلفنی'},
          { key: index++, label: 'نوشتاری'},
          { key: index++, label: 'حضوری'}
        ];
        const data2 = [
          { key: index++, label: 'تهران'},
          { key: index++, label: 'مشهد'},
          { key: index++, label: 'شیراز'},
          { key: index++, label: 'اصفهان'},
          { key: index++, label: 'تبریز'}
        ];
        const data3 = [
          { key: index++, label: 'مشاور خانواده'},
          { key: index++, label: 'مشاور ازدواج'},
          { key: index++, label: 'مشاور کاری'},
        ];
        const {navigate}=this.props.navigation;
        return (
            <Drawer
                ref={(ref)=>{this.drawer=ref;}}
                content={<SideBar navigate={navigate}/>}
                onClose={()=>this.closeDrawer()}
            >
                <Container>
                    <Header style={{backgroundColor:'#3f4392'}}>
                        <Left>
                            <Grid>
                                <Col style={{width:'20%'}}>
                                    <Button transparent onPress={()=>this.props.navigation.goBack()}>
                                        <Icon style={{color:'#FFF'}} name="ios-arrow-back" size={30} type="Ionicons"/>
                                    </Button>
                                </Col>
                                <Col style={{width:'50%'}}>
                                    <Button transparent title="Show modal" onPress={this.toggleModal} >
                                        <Icon name="search" type="FontAwesome" style={{color:'#FFF',fontSize:30}} />
                                    </Button>
                                    <Modal isVisible={this.state.isModalVisible}>
                                        <View style={{ flex: 1,paddingTop:'30%',}}>
                                            <View style={{backgroundColor:'#FFF',borderRadius:20}}>
                                                <Button
                                                    style={{
                                                        backgroundColor:'#3f4392',
                                                        borderRadius:20,
                                                        borderBottomLeftRadius:0,
                                                        borderBottomRightRadius:0,
                                                    }}
                                                    title="Hide modal" onPress={this.toggleModal} 
                                                >
                                                <Icon style={{color:'#FFF',fontSize:25,marginLeft: 10,}} name="close" type="AntDesign"/>
                                                </Button>
                                                <Card style={{paddingBottom:'5%',paddingTop:'5%'}}>
                                                    <Thumbnail source={require('../img/hs.jpg')} style={{marginLeft:'43%',}}/>
                                                    <Text style={{textAlign:'center',fontWeight:'bold',marginTop:15}}>سید حسین سعیدی</Text>
                                                    <Text style={{textAlign:'center',marginTop:5}}>مشاور حقوقی</Text>
                                                </Card>
                                                <ModalSelector
                                                    data={data}
                                                    initValue="انتخاب نوع مشاوره"
                                                    style={[styles.ModalSelector]}
                                                />
                                                <ModalSelector
                                                    data={data2}
                                                    initValue="انتخاب شهر مشاور"
                                                    style={[styles.ModalSelector]}
                                                />
                                                <ModalSelector
                                                    data={data3}
                                                    initValue="انتخاب تخصص مشاور"
                                                    style={[styles.ModalSelector]}
                                                />
                                                <Button full style={{backgroundColor:'#3f4392',fontSize:15,margin:5,borderBottomLeftRadius:20,borderBottomEndRadius:20}}>
                                                    <Text>جستجو</Text>
                                                </Button>
                                            </View>  
                                        </View>
                                    </Modal>
                                </Col>
                            </Grid>
                        </Left>
                        <Right>
                            <Grid>
                                <Col style={{width:'75%'}}>
                                    <Button style={{backgroundColor:'transparent',}} onPress={()=>this.props.navigate('Home')} >
                                        <Image source={require('../img/topmo1.png')}/>
                                    </Button>
                                </Col>
                                <Col style={{width:'25%'}}>
                                    <Button transparent onPress={()=>this.openDrawer()}>
                                        <Icon name="navicon" type="FontAwesome" style={{color:'#FFF'}}/>
                                    </Button>
                                </Col>
                            </Grid>
                        </Right>
                    </Header>
                    <Content>
                        <View style={{borderBottomWidth:2,borderBottomColor:'#3f4392',marginTop:30,marginBottom:25}}></View>
                        <View style={{position:'absolute',width:'30%',marginVertical: '70%',}}>
                            <Text style={{textAlign:'center',backgroundColor:'#fff',marginTop:18,color:'#3f4392',fontWeight:'bold'}}>پروفایل</Text>
                        </View>
                        <View style={[styles.Cardstyle,{}]}>
                            <Left/>
                            <Body>
                                <Thumbnail source={require('../img/hs.jpg')}/>
                                <Text style={{marginTop:10}}>
                                     سید حسین  
                                     <Text> سعیدی</Text>
                                </Text>
                                <Text note style={{marginTop:5}}>موجودی کیف پول :<Text note> ۱۰/۰۰۰ تومان</Text></Text>
                            </Body>
                            <Right/>
                        </View>
                        <View style={{marginTop:2.5}}>
                            <View style={[styles.GridStyle,{}]}>
                                <Text note style={{textAlign:'right'}}>بیوگرافی</Text>
                                <Text style={{textAlign:'right'}}>طراح وب سایت</Text>
                                <Text style={{textAlign:'right'}}>طراح وب سایت طراح اپلیکیشن</Text>
                            </View>
                            <Grid style={[styles.GridStyle,{}]}>
                                <Col>
                                    <Text style={{textAlign:'left'}}>gmail@gmail.Com</Text>
                                </Col>
                                <Col>
                                    <Text style={{textAlign:'right'}}>کاربر / مشاور</Text>
                                </Col>
                            </Grid>
                            <Grid style={[styles.GridStyle,{}]}>
                                <Col>
                                    <Text style={{textAlign:'left'}}>۰۹۱۲۳۴۵۶۷۸۹</Text>
                                </Col>
                                <Col>
                                    <Text style={{textAlign:'right'}}>کد تخفیف :<Text>۱۲۳۴</Text></Text>
                                </Col>
                            </Grid>
                            <Grid>
                                <Col>
                                    <Button full style={[styles.buttonStyle]} onPress={()=>this.props.navigation.navigate('MyDetails')}>
                                        <Text style={[styles.Text]}>ویرایش اطلاعات</Text>
                                        <Icon style={{color:'#3f4392'}} name="account-edit" type="MaterialCommunityIcons" />
                                    </Button>
                                </Col>
                                <Col>
                                    <Button full style={[styles.buttonStyle,{backgroundColor:'#3f4392'}]} onPress={()=>this.props.navigation.navigate('Biography')}>
                                        <Text style={[styles.Text,{color:'#61d6cd'}]}>بیوگرافی</Text>
                                        <Icon style={{color:'#61d6cd'}} name="address-book" type="FontAwesome" />
                                    </Button>
                                </Col>
                            </Grid>
                            <Grid>
                                <Col>
                                    <Button full style={[styles.buttonStyle,{backgroundColor:'#3f4392'}]} onPress={()=>this.props.navigation.navigate('Program')}>
                                        <Text style={[styles.Text,{color:'#61d6cd'}]}>برنامه کاری</Text>
                                        <Icon style={{color:'#61d6cd'}} name="book-bookmark" type="Foundation" />
                                    </Button>
                                </Col>
                                <Col>
                                    <Button full style={[styles.buttonStyle]} onPress={()=>this.props.navigation.navigate('Reservations')}>
                                        <Text style={[styles.Text]}>رزروها</Text>
                                        <Icon style={{color:'#3f4392'}} name="list-alt" type="FontAwesome" />
                                    </Button>
                                </Col>
                            </Grid>
                            <Grid>
                                <Col>
                                    <Button full style={[styles.buttonStyle]} onPress={()=>this.props.navigation.navigate('Charge')}>
                                        <Text style={[styles.Text]}>افزایش اعتبار</Text>
                                        <Icon style={{color:'#3f4392'}} name="credit-card" type="FontAwesome" />
                                    </Button>
                                </Col>
                                <Col>
                                    <Button full style={[styles.buttonStyle,{backgroundColor:'#3f4392'}]} onPress={()=>this.props.navigation.navigate('MyDocuments')}>
                                        <Text style={[styles.Text,{color:'#61d6cd'}]}>مدارک من</Text>
                                        <Icon style={{color:'#61d6cd'}} name="file-document-box-multiple" type="MaterialCommunityIcons" />
                                    </Button>
                                </Col>
                            </Grid>
                            <Grid>
                                <Col>
                                    <Button full style={[styles.buttonStyle,{backgroundColor:'red'}]} >
                                        <Text style={[styles.Text,{color:'#FFF'}]}>خروج از حساب</Text>
                                        <Icon style={{color:'#FFF'}} name="logout" type="AntDesign" />
                                    </Button>
                                </Col>
                            </Grid>
                        </View>
                    </Content>
                    <Footer style={styles.footerStyle}>
                        <FooterTab>
                            <Button vertical onPress={()=>this.props.navigation.navigate('Profile')}>
                                <Icon name="user" type="Entypo" style={{color:'#fff',fontSize:30}}/>
                                <Text style={styles.textStyle}>پروفایل</Text>
                            </Button>
                            <Button vertical onPress={()=>this.props.navigation.navigate('Consultantslist')}>
                                <Icon name="perm-contact-calendar" type="MaterialIcons" style={{color:'#fff',fontSize:30}}/>
                                <Text style={styles.textStyle}>مشاوران</Text>
                            </Button>
                            <Button vertical onPress={()=>this.props.navigation.navigate('Map')}>
                                <Icon name="map-marked-alt" type="FontAwesome5" style={{color:'#fff',fontSize:30}}/>
                                <Text style={styles.textStyle}>نقشه</Text>
                            </Button>
                            <Button vertical onPress={()=>this.props.navigation.navigate('Reports')}>
                                <Icon name="md-list-box" type="Ionicons" style={{color:'#fff',fontSize:30}}/>
                                <Text style={styles.textStyle}>گزارشات</Text>
                            </Button>
                            <Button vertical onPress={()=>this.props.navigation.navigate('Home')}>
                                <Icon name="home" type="FontAwesome" style={{color:'#fff',fontSize:30}}/>
                                <Text style={styles.textStyle}>خانه</Text>
                            </Button>
                        </FooterTab>
                    </Footer>
                </Container>
            </Drawer>
        );
    }
}

const styles = StyleSheet.create({
    footerStyle:{
        backgroundColor:'#3f4392',
        height:65,
    },
    textStyle:{
        color:'#FFF',
        marginTop:3,
        fontSize:13,
    },
    buttonStyle:{
        backgroundColor:'#61d6cd',
        margin:2,
        borderRadius:15,
        borderColor: '#ddd',
        borderBottomWidth: 0,
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.5,
        shadowRadius: 2,
        elevation: 1,
        marginLeft: 5,
        marginRight: 5,
        marginTop: 10,
    },
    Text:{
        color:'#3f4392',
        fontWeight: 'bold',
    },
    Cardstyle:{
        paddingVertical:20,
    },
    Colstyle:{
        width:'50%'
    },
    GridStyle:{
        borderColor: '#ddd',
        borderBottomWidth: 1,
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.3,
        shadowRadius: 2,
        elevation: 1,
        paddingVertical: 10,
        paddingRight:10,
        paddingLeft: 10,
    }
})

export {ProfileComponent};
