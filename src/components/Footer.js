import React, { Component } from 'react';
import { StyleSheet} from 'react-native';
import { Button, Footer, FooterTab, Text } from 'native-base';
class FooterComponent extends Component {
    constructor(props){
        super(props);
    }
    render() {
        return (
            <Footer style={styles.footerStyle}>
                <FooterTab>
                    <Button vertical onPress={()=>this.props.navigation.navigate('Profile')}>
                        <Icon name="user" type="Entypo" style={{color:'#fff',fontSize:30}}/>
                        <Text style={styles.textStyle}>پروفایل</Text>
                    </Button>
                    <Button vertical onPress={()=>this.props.navigation.navigate('Consultantslist')}>
                        <Icon name="perm-contact-calendar" type="MaterialIcons" style={{color:'#fff',fontSize:30}}/>
                        <Text style={styles.textStyle}>مشاوران</Text>
                    </Button>
                    <Button vertical onPress={()=>this.props.navigation.navigate('Map')}>
                        <Icon name="map-marked-alt" type="FontAwesome5" style={{color:'#fff',fontSize:30}}/>
                        <Text style={styles.textStyle}>نقشه</Text>
                    </Button>
                    <Button vertical onPress={()=>this.props.navigation.navigate('Reports')}>
                        <Icon name="md-list-box" type="Ionicons" style={{color:'#fff',fontSize:30}}/>
                        <Text style={styles.textStyle}>گزارشات</Text>
                    </Button>
                    <Button vertical onPress={()=>this.props.navigation.navigate('Home')}>
                        <Icon name="home" type="FontAwesome" style={{color:'#fff',fontSize:30}}/>
                        <Text style={styles.textStyle}>خانه</Text>
                    </Button>
                </FooterTab>
            </Footer>
        );
    }
}

const styles = StyleSheet.create({
    footerStyle:{
        backgroundColor:'#3f4392',
        height:65,
    },
    textStyle:{
        color:'#FFF',
        marginTop:3,
        fontSize:13,
    },
})

export {FooterComponent};