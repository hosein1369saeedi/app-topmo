
import React, {Component} from 'react';
import {StyleSheet,Image} from 'react-native';
import {Container,Header, Left, Button,Text, Body, Grid, Right, Content, Footer, FooterTab, Drawer, Icon, List, ListItem, Switch, Thumbnail, Col, View, Card} from 'native-base';
import {SideBar} from './index';
import ModalSelector from 'react-native-modal-selector';
import Modal from 'react-native-modal';
import { Badge } from 'react-native-elements';

class SettingComponent extends Component {
    state = {
        isModalVisible: false
    };
    constructor(props){
        super(props);
        this.state={
          textInputValue: ''
        }
    }
    toggleModal = () => {
        this.setState({ isModalVisible: !this.state.isModalVisible });
    };
    closeDrawer=()=>{
        this.drawer._root.close()
    }
    openDrawer=()=>{
        this.drawer._root.open()
    }
    render() {
        let index = 0;
        const data = [
            { key: index++, label: 'تلفنی'},
            { key: index++, label: 'نوشتاری'},
            { key: index++, label: 'حضوری'}
        ];
        const data2 = [
            { key: index++, label: 'تهران'},
            { key: index++, label: 'مشهد'},
            { key: index++, label: 'شیراز'},
            { key: index++, label: 'اصفهان'},
            { key: index++, label: 'تبریز'}
        ];
        const data3 = [
            { key: index++, label: 'مشاور خانواده'},
            { key: index++, label: 'مشاور ازدواج'},
            { key: index++, label: 'مشاور کاری'},
        ];
        const {navigate}=this.props.navigation;
        return (
            <Drawer
            ref={(ref)=>{this.drawer=ref;}}
            content={<SideBar navigate={navigate}/>}
            onClose={()=>this.closeDrawer()}
            >
                <Container>
                    <Header style={{backgroundColor:'#3f4392'}}>
                        <Left>
                            <Grid>
                                <Col style={{width:'20%'}}>
                                    <Button transparent onPress={()=>this.props.navigation.goBack()}>
                                        <Icon style={{color:'#FFF'}} name="ios-arrow-back" size={30} type="Ionicons"/>
                                    </Button>
                                </Col>
                                <Col style={{width:'50%'}}>
                                    <Button transparent title="Show modal" onPress={this.toggleModal} >
                                        <Icon name="search" type="FontAwesome" style={{color:'#FFF',fontSize:30}} />
                                    </Button>
                                    <Modal isVisible={this.state.isModalVisible}>
                                        <View style={{ flex: 1,paddingTop:'30%',}}>
                                            <View style={{backgroundColor:'#FFF',borderRadius:20}}>
                                                <Button
                                                    style={{
                                                        backgroundColor:'#3f4392',
                                                        borderRadius:20,
                                                        borderBottomLeftRadius:0,
                                                        borderBottomRightRadius:0,
                                                        width:'100%'
                                                    }}
                                                    title="Hide modal" onPress={this.toggleModal} 
                                                >
                                                    <Icon style={{color:'#FFF',fontSize:25,marginLeft: 10,}} name="close" type="AntDesign"/>
                                                </Button>
                                                <Card style={{paddingBottom:'5%',paddingTop:'5%'}}>
                                                    <Thumbnail source={require('../img/hs.jpg')} style={{marginLeft:'43%',}}/>
                                                    <Text style={{textAlign:'center',fontWeight:'bold',marginTop:15}}>سید حسین سعیدی</Text>
                                                    <Text style={{textAlign:'center',marginTop:5}}>مشاور حقوقی</Text>
                                                </Card>
                                                <ModalSelector
                                                    data={data}
                                                    initValue="انتخاب نوع مشاوره"
                                                    stylقe={[styles.ModalSelector]}
                                                />
                                                <ModalSelector
                                                    data={data2}
                                                    initValue="انتخاب شهر مشاور"
                                                    style={[styles.ModalSelector]}
                                                />
                                                <ModalSelector
                                                    data={data3}
                                                    initValue="انتخاب تخصص مشاور"
                                                    style={[styles.ModalSelector]}
                                                />
                                                <Button full style={{backgroundColor:'#3f4392',fontSize:15,margin:5,borderBottomLeftRadius:20,borderBottomEndRadius:20}}>
                                                    <Text>جستجو</Text>
                                                </Button>
                                            </View>  
                                        </View>
                                    </Modal>
                                </Col>
                            </Grid>
                        </Left>
                        <Right>
                            <Grid>
                                <Col style={{width:'75%'}}>
                                    <Button style={{backgroundColor:'transparent',}} onPress={()=>this.props.navigate('Home')} >
                                        <Image source={require('../img/topmo1.png')}/>
                                    </Button>
                                </Col>
                                <Col style={{width:'25%'}}>
                                    <Button transparent onPress={()=>this.openDrawer()}>
                                        <Icon name="navicon" type="FontAwesome" style={{color:'#FFF'}}/>
                                    </Button>
                                </Col>
                            </Grid>
                        </Right>
                    </Header>
                    <Content>
                        <List>
                            <ListItem avatar>
                                <Left>
                                    <Button transparent onPress={()=>this.props.navigation.navigate('Profile')}>
                                        <Icon style={{color:'#000',fontSize:15}} name="eye" type="FontAwesome"/>
                                    </Button>
                                </Left>
                                <Body>
                                    <Text style={{textAlign:'right',fontWeight:'bold',}}>سید حسین سعیدی</Text>
                                    <Text style={{textAlign:'right'}} note>مشاور حقوقی</Text>
                                    <Text style={{textAlign:'right'}} note>۰۹۱۲۳۴۵۶۷۸۹</Text>
                                </Body>
                                <Right>
                                    <Thumbnail source={require('../img/hs.jpg')} />
                                </Right>
                            </ListItem>
                        </List>
                        <List>
                            <ListItem itemDivider>
                                <Left></Left>
                                <Body>
                                    <Text style={{textAlign:'right',color:'#999'}}>وضعیت</Text>
                                </Body>
                            </ListItem>
                            <ListItem icon>
                                <Left>
                                    <Switch value={true} />
                                </Left>
                                <Body>
                                    <Text style={{textAlign:'right'}}>آنلاین هستم</Text>
                                </Body>
                                <Right>
                                    <Icon style={{color:'#999'}} name="wifi" />
                                </Right>
                            </ListItem>
                        </List>
                        <List>
                            <ListItem icon>
                                <Left>
                                    <Button transparent onPress={()=>this.props.navigation.navigate('')}>
                                        <Icon style={{color:'#000',fontSize:15}} name="eye" type="FontAwesome"/>
                                    </Button>
                                </Left>
                                <Body>
                                    <Text style={{textAlign:'right'}}>صورتحساب</Text>
                                </Body>
                                <Right>
                                    <Icon style={{color:'#999'}} name="format-list-numbered" type="MaterialIcons" />
                                </Right>
                            </ListItem>
                        </List>
                        <List>
                            <ListItem itemDivider>
                                <Left></Left>
                                <Body>
                                    <Text style={{textAlign:'right',color:'#999'}}>تاپمو</Text>
                                </Body>
                            </ListItem>
                            <ListItem icon>
                                <Left/>
                                <Body>
                                    <Text style={{textAlign:'right'}}>اشتراک گذاری اپلیکیشن</Text>
                                </Body>
                                <Right>
                                    <Icon style={{color:'#999'}} name="share" type="Entypo" />
                                </Right>
                            </ListItem>
                        </List>
                        <List>
                            <ListItem icon>
                                <Left>
                                    <Button transparent onPress={()=>this.props.navigation.navigate('')}>
                                        <Icon style={{color:'#000',fontSize:15}} name="eye" type="FontAwesome"/>
                                    </Button>
                                </Left>
                                <Body>
                                    <Text style={{textAlign:'right'}}>راهنما اپلیکیشن</Text>
                                </Body>
                                <Right>
                                    <Icon style={{color:'#999'}} name="screen-smartphone" type="SimpleLineIcons" />
                                </Right>
                            </ListItem>
                        </List>
                    </Content>
                    <Footer style={styles.footerStyle}>
                        <FooterTab>
                            <Button vertical onPress={()=>this.props.navigation.navigate('Profile')}>
                                <Icon name="user" type="Entypo" style={{color:'#fff',fontSize:30}}/>
                                <Text style={styles.textStyle}>پروفایل</Text>
                            </Button>
                            <Button vertical onPress={()=>this.props.navigation.navigate('Consultantslist')}>
                                <Icon name="perm-contact-calendar" type="MaterialIcons" style={{color:'#fff',fontSize:30}}/>
                                <Text style={styles.textStyle}>مشاوران</Text>
                            </Button>
                            <Button vertical onPress={()=>this.props.navigation.navigate('Map')}>
                                <Icon name="map-marked-alt" type="FontAwesome5" style={{color:'#fff',fontSize:30}}/>
                                <Text style={styles.textStyle}>نقشه</Text>
                            </Button>
                            <Button vertical onPress={()=>this.props.navigation.navigate('Reports')}>
                                <Icon name="md-list-box" type="Ionicons" style={{color:'#fff',fontSize:30}}/>
                                <Text style={styles.textStyle}>گزارشات</Text>
                            </Button>
                            <Button vertical onPress={()=>this.props.navigation.navigate('Home')}>
                                <Icon name="home" type="FontAwesome" style={{color:'#fff',fontSize:30}}/>
                                <Text style={styles.textStyle}>خانه</Text>
                            </Button>
                        </FooterTab>
                    </Footer>
                </Container>
            </Drawer>
        );
    }
}

const styles = StyleSheet.create({
    footerStyle:{
        backgroundColor:'#3f4392',
        height:65,
    },
    textStyle:{
        color:'#FFF',
        marginTop:3,
        fontSize:13,
    },
})

export {SettingComponent}
