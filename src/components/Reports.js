
import React, {Component} from 'react';
import {StyleSheet,Image,View} from 'react-native';
import {Container,Header, Left, Button,Text, Card, Grid, Right, Footer, FooterTab, Drawer, Icon, Thumbnail, Col, Content, CardItem } from 'native-base';
import {SideBar} from './index';
import Modal from 'react-native-modal';
import ModalSelector from 'react-native-modal-selector';

class ReportsComponent extends Component {
    state = {
        isModalVisible: false
    }
    toggleModal = () => {
        this.setState({ isModalVisible: !this.state.isModalVisible });
    }
    constructor(props){
        super(props);
        this.state={
            selected: "key0",
            textInputValue: ''
        }
    }
    onValueChange(value: string) {
        this.setState({
            selected: value
        });
    }
    closeDrawer=()=>{
        this.drawer._root.close()
    }
    openDrawer=()=>{
        this.drawer._root.open()
    }
    render() {
        let index = 0;
        const data = [
          { key: index++, label: 'تلفنی'},
          { key: index++, label: 'نوشتاری'},
          { key: index++, label: 'حضوری'}
        ];
        const data2 = [
          { key: index++, label: 'تهران'},
          { key: index++, label: 'مشهد'},
          { key: index++, label: 'شیراز'},
          { key: index++, label: 'اصفهان'},
          { key: index++, label: 'تبریز'}
        ];
        const data3 = [
          { key: index++, label: 'مشاور خانواده'},
          { key: index++, label: 'مشاور ازدواج'},
          { key: index++, label: 'مشاور کاری'},
        ];
        const {navigate}=this.props.navigation;
        return (
            <Drawer
                ref={(ref)=>{this.drawer=ref;}}
                content={<SideBar navigate={navigate}/>}
                onClose={()=>this.closeDrawer()}
            >
                <Container style={{backgroundColor:'#DDD'}}>
                    <Header style={{backgroundColor:'#3f4392'}}>
                        <Left>
                            <Grid>
                                <Col style={{width:'20%'}}>
                                    <Button transparent onPress={()=>this.props.navigation.goBack()}>
                                        <Icon style={{color:'#FFF'}} name="ios-arrow-back" size={30} type="Ionicons"/>
                                    </Button>
                                </Col>
                                <Col style={{width:'50%'}}>
                                    <Button transparent title="Show modal" onPress={this.toggleModal} >
                                        <Icon name="ios-search" style={{color:'#FFF'}} />
                                    </Button>
                                    <Modal isVisible={this.state.isModalVisible}>
                                        <View style={{ flex: 1,paddingTop:'30%',}}>
                                            <View style={{backgroundColor:'#FFF',borderRadius:20}}>
                                                <Button 
                                                    style={{
                                                        backgroundColor:'#3f4392',
                                                        borderRadius:20,
                                                        borderBottomLeftRadius:0,
                                                        borderBottomRightRadius:0,
                                                    }}
                                                    title="Hide modal" onPress={this.toggleModal} 
                                                >
                                                    <Icon style={{color:'#FFF',fontSize:25,marginLeft: 10,}} name="close" type="AntDesign"/>
                                                </Button>
                                                <Card style={{paddingBottom:'5%',paddingTop:'5%'}}>
                                                    <Thumbnail source={require('../img/hs.jpg')} style={{marginLeft:'43%',}}/>
                                                    <Text style={{textAlign:'center',fontWeight:'bold',marginTop:15}}>سید حسین سعیدی</Text>
                                                    <Text style={{textAlign:'center',marginTop:5}}>مشاور حقوقی</Text>
                                                </Card>
                                                <ModalSelector
                                                    data={data}
                                                    initValue="انتخاب نوع مشاوره"
                                                    style={[styles.ModalSelector]}
                                                />
                                                <ModalSelector
                                                    data={data2}
                                                    initValue="انتخاب شهر مشاور"
                                                    style={[styles.ModalSelector]}
                                                />
                                                <ModalSelector
                                                    data={data3}
                                                    initValue="انتخاب تخصص مشاور"
                                                    style={[styles.ModalSelector]}
                                                />
                                                <Button full style={{backgroundColor:'#3f4392',fontSize:15,margin:5,borderRadius:20}}>
                                                    <Text>جستجو</Text>
                                                </Button>
                                            </View>  
                                        </View>
                                    </Modal>
                                </Col>
                            </Grid>
                        </Left>
                        <Right>
                            <Grid>
                                <Col style={{width:'75%'}}>
                                    <Button style={{backgroundColor:'transparent',}} onPress={()=>this.props.navigate('Home')} >
                                        <Image source={require('../img/topmo1.png')}/>
                                    </Button>
                                </Col>
                                <Col style={{width:'25%'}}>
                                    <Button transparent onPress={()=>this.openDrawer()}>
                                        <Icon name="navicon" type="FontAwesome" style={{color:'#FFF'}}/>
                                    </Button>
                                </Col>
                            </Grid>
                        </Right>
                    </Header>
                    <Content style={{backgroundColor:'#DDD'}}>
                        <View style={{borderBottomWidth:2,borderBottomColor:'#3f4392',marginTop:30,marginBottom:25}}></View>
                        <View style={{position:'absolute',marginLeft: '33%',}}>
                            <Text style={{textAlign:'center',backgroundColor:'#DDD',marginTop:18,color:'#3f4392',fontWeight:'bold',}}>گزارشات</Text>
                        </View>
                        <View>
                            <Card style={[{paddingVertical:'6%',shadowColor:'transparent',borderColor:'transparent',backgroundColor:'#DDD'}]}>
                                <CardItem style={[styles.CardItemStyle,{marginVertical:20,}]}>
                                    <Grid>
                                        <Col style={{width:'80%',direction:'rtl'}}>
                                            <Button style={[styles.buttonStyle,{}]} onPress={()=>this.props.navigation.navigate('ReportsFinancial')}>
                                                <Text style={[styles.buyStyle,{color:'#3f4392',marginLeft:15}]}>"گزارشات مالی"</Text>
                                            </Button>
                                        </Col>
                                        <Col style={{width:'20%',borderRadius:10}}>
                                            <View>
                                                <Image source={require('../img/wallet.png')} style={[styles.imgStyle,{}]}/>
                                            </View>
                                        </Col>
                                    </Grid>
                                </CardItem>
                                <CardItem style={[styles.CardItemStyle,{backgroundColor:'#3f4392',marginVertical:20}]}>
                                    <Grid>
                                        <Col style={{width:'80%',direction:'rtl'}}>
                                            <Button style={[styles.buttonStyle,{}]} onPress={()=>this.props.navigation.navigate('SupportReports')}>
                                                <Text style={[styles.buyStyle,{color:'#61d6cd'}]}>"گزارشات پشتیبانی"</Text>
                                            </Button>
                                        </Col>
                                        <Col style={{width:'20%'}}>
                                            <View>
                                                <Image source={require('../img/sopport.png')} style={[styles.imgStyle,{}]}/>
                                            </View>
                                        </Col>
                                    </Grid>
                                </CardItem>
                                <CardItem style={[styles.CardItemStyle,{marginVertical:20}]}>
                                    <Grid>
                                        <Col style={{width:'80%',direction:'rtl'}}>
                                            <Button style={[styles.buttonStyle,{}]} onPress={()=>this.props.navigation.navigate('')}>
                                                <Text style={[styles.buyStyle,{color:'#3f4392'}]}>"گزارشات مشاوره"</Text>
                                            </Button>
                                        </Col>
                                        <Col style={{width:'20%'}}>
                                            <View>
                                                <Image source={require('../img/question.png')} style={[styles.imgStyle,{}]}/>
                                            </View>
                                        </Col>
                                    </Grid>
                                </CardItem>
                            </Card>
                        </View>
                    </Content>
                    <Footer style={styles.footerStyle}>
                        <FooterTab>
                            <Button vertical onPress={()=>this.props.navigation.navigate('Profile')}>
                                <Icon name="user" type="Entypo" style={{color:'#fff',fontSize:30}}/>
                                <Text style={styles.textStyle}>پروفایل</Text>
                            </Button>
                            <Button vertical onPress={()=>this.props.navigation.navigate('Consultantslist')}>
                                <Icon name="perm-contact-calendar" type="MaterialIcons" style={{color:'#fff',fontSize:30}}/>
                                <Text style={styles.textStyle}>مشاوران</Text>
                            </Button>
                            <Button vertical onPress={()=>this.props.navigation.navigate('Map')}>
                                <Icon name="map-marked-alt" type="FontAwesome5" style={{color:'#fff',fontSize:30}}/>
                                <Text style={styles.textStyle}>نقشه</Text>
                            </Button>
                            <Button vertical onPress={()=>this.props.navigation.navigate('Reports')}>
                                <Icon name="md-list-box" type="Ionicons" style={{color:'#fff',fontSize:30}}/>
                                <Text style={styles.textStyle}>گزارشات</Text>
                            </Button>
                            <Button vertical onPress={()=>this.props.navigation.navigate('Home')}>
                                <Icon name="home" type="FontAwesome" style={{color:'#fff',fontSize:30}}/>
                                <Text style={styles.textStyle}>خانه</Text>
                            </Button>
                        </FooterTab>
                    </Footer>
                </Container>
            </Drawer>
        );
    }
}

const styles = StyleSheet.create({
    footerStyle:{
        backgroundColor:'#3f4392',
        height:65,
    },
    textStyle:{
        color:'#FFF',
        marginTop:3,
        fontSize:13,
    },
    CardItemStyle:{
        margin:5,
        marginTop:'1%',
        borderRadius:10,
        borderColor: '#3f4392',
        borderBottomWidth: 0,
        shadowColor: '#3f4392',
        shadowOffset: { width: 0, height: 3 },
        shadowOpacity: 0.6,
        shadowRadius: 2,
        elevation: 1,
        height:100,
        backgroundColor:'#61d6cd',
    },
    buttonStyle:{
        width:"90%",
        height:40,
        padding:10,
        marginTop:20,
        backgroundColor:'transparent',
        textAlign:'center',
        paddingLeft:'20%',
    },
    buyStyle:{
        color:'#3f4392',
        fontSize:17,
        textAlign:'center',
        fontWeight:'bold'
    },
    imgStyle:{
        borderRadius:10,
        width:80,
        height:80,
        marginTop:2,
        marginLeft:-12,
    },
})

export {ReportsComponent}
